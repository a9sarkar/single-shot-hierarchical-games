'''
Created on Apr 13, 2021

@author: Atrisha
'''

from planners.trajectory_planner import TrajectoryPlanner, VehicleTrajectoryPlanner, PedestrianTrajectoryPlanner
from planners.trajectory_planner import WaitTrajectoryConstraints, ProceedTrajectoryConstraints, PedestrianManeuverConstraints
from planners.planning_objects import TrajectoryConstraintsFactory
from motion_planners.planning_objects import VehicleState
from maps.States import SyntheticScenarioDef, InvalidScenarioException
import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import constants
from maps.States import ScenarioDef
from equilibrium.game_tree import Actions
from visualizer.visualizer import plot_traffic_regions
from rg_visualizer import UniWeberAnalytics
from shapely.geometry import MultiPoint
import all_utils
import matplotlib
from shapely.geometry import Polygon
from matplotlib.patches import Polygon as MPLPolygon
from equilibrium.utilities import Utilities 
import xml.etree.ElementTree as ET
import ast
from pyproj import Proj
import itertools
from shapely.geometry import LineString
import rg_constants
import all_utils.utils as base_utils
from matplotlib.collections import PatchCollection
import ss_constants
import os
import csv
import ast
import json
from game_classes import *


WAIT_ACTIONS = ['yield-to-merging','wait_for_lead_to_cross','wait-for-oncoming','decelerate-to-stop','wait-on-red','wait-for-pedestrian']

class GameGenerator():
    
    def generate(self):
        cache_path = os.path.join('D:\\oneshot_games_data','intersection_dataset','game_trees')
        with open(rg_constants.SCENE_OUT_PATH,newline='\n') as csv_file:
            sc_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in sc_reader:
                line_count += 1
                file_id = row[0]
                row_sc_type = row[1]
                row_dir = row[2]
                agent_1_id = int(row[3])
                agent_2_id = int(row[4])
                start_ts = float(row[5])
                '''
                if os.path.isfile(os.path.join(cache_path,'_'.join([file_id,str(agent_1_id),str(agent_2_id),str(start_ts)])+'.json')):
                    print('row',row,'processed...continuing')
                    continue
                '''
                all_agents = [agent_1_id,agent_2_id]
                constants.PLAN_HORIZON_SECS = 6
                try:
                    scene_def = ScenarioDef(agent_1_id=agent_1_id, agent_2_id=agent_2_id,file_id=file_id,initialize_db=True,start_ts= start_ts,freq=0.5,one_shot_scenario=True)
                    if scene_def.horizon < 1:
                        raise InvalidScenarioException('horizon too short')
                except InvalidScenarioException:
                    print('row',row,'Invalid scenario...continuing')
                    continue
                if scene_def.time_crossed:
                    print('failed')
                    raise Exception()
                else:
                    ag1_obj = scene_def.agent1
                    ag2_obj = scene_def.agent2
                '''
                scene_def = ScenarioDef(agent_1_id=43, agent_2_id=None,file_id='769',initialize_db=False,start_ts=43.043,freq=0.5)
                if scene_def.time_crossed:
                    print('failed')
                    raise Exception()
                else:
                    lead_ag_obj = scene_def.agent
                '''
                lead_ag_obj = None
                #fig, ax = plt.subplots(1,1)
                ag1_traj, ag2_traj = None, None
                l2tag_map, l2key_map = {'lb':0,'p':1,'ub':2}, {0:'lb',1:'p',2:'ub'}
                traj_dict = {agent_1_id:{},agent_2_id:{}}
                emp_traj_errs = [(None,np.inf,scene_def.agent1_emp_traj[-1]), (None,np.inf,scene_def.agent2_emp_traj[-1])]
                try:
                    for ag1_obj_manv in ['proceed-turn','wait-for-oncoming']:
                        code = base_utils.unreadable('|'.join([str(agent_1_id),'0',ag1_obj_manv,'NORMAL']))
                        constr = TrajectoryConstraintsFactory.get_constraint_object(maneuver=ag1_obj_manv, ag_obj=ag1_obj, lead_ag_obj=lead_ag_obj)
                        #constr.set_limit_constraints()
                        constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=22,max_acc_lims=5,max_jerk_lims=3)
                        agent1_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver= ag1_obj_manv, mode=None, horizon=6)
                        agent1_motion.generate_trajectory(True)
                        if max([len(x) for x in agent1_motion.all_trajectories.values()]) == 0:
                            continue
                        all_trajs = []
                        for k,v in agent1_motion.all_trajectories.items():
                            #print('generated',len(v),k,'trajectories for',ag1_obj_manv)
                            if len(v) == 0:
                                continue
                            '''
                            for traj in v:
                                ax[0].plot([float(x[0]) for x in traj],[float(x[3])*3.6 for x in traj],'-',color='red' if ag1_obj_manv == 'proceed-turn' else 'blue')
                                print(float(traj[-1][3])*3.6)
                            if ag1_obj_manv == selected_outcome[0]:
                                ag1_traj = v[-1]
                            '''
                            _this_trajs = [[_ for _ in x if _[0] <= scene_def.horizon] for x in v]
                            all_trajs += _this_trajs
                            _err = [LineString([(x[1],x[2]) for x in _t]).length - emp_traj_errs[0][2] for _t in _this_trajs] 
                            min_abs_err = min([abs(x) for x in _err])
                            if min_abs_err < abs(emp_traj_errs[0][1]):
                                emp_traj_errs[0] = (ag1_obj_manv,min_abs_err,emp_traj_errs[0][2])
                        _errs = sorted([LineString([(t[1],t[2]) for t in x]).length-emp_traj_errs[0][2] for x in all_trajs])
                        _length_sorted = sorted(all_trajs, key=lambda x: LineString([(t[1],t[2]) for t in x]).length)
                        all_trajs = sorted(all_trajs, key=lambda x: LineString([(t[1],t[2]) for t in x]).length-emp_traj_errs[0][2])
                        if ag1_obj_manv not in traj_dict[agent_1_id]:
                            lb_idx,p_idx,ub_idx = np.argmin(_errs),np.argmin([abs(x) for x in _errs]),np.argmax(_errs)
                            traj_lb, traj_p, traj_ub = _length_sorted[0],all_trajs[p_idx],_length_sorted[-1]
                            trajl_lb, trajl_p, trajl_ub = LineString([(x[1],x[2]) for x in traj_lb]).length, LineString([(x[1],x[2]) for x in traj_p]).length, LineString([(x[1],x[2]) for x in traj_ub]).length
                            traj_dict[agent_1_id][ag1_obj_manv] = ([traj_lb, traj_p, traj_ub],[trajl_lb, trajl_p, trajl_ub])
                        
                    for ag2_obj_manv in ['track_speed','decelerate-to-stop']:
                        constr = TrajectoryConstraintsFactory.get_constraint_object(maneuver=ag2_obj_manv, ag_obj=ag2_obj, lead_ag_obj=lead_ag_obj)
                        #constr.set_limit_constraints()
                        constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=22,max_acc_lims=5,max_jerk_lims=3)
                        agent2_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver= ag2_obj_manv, mode=None, horizon=6)
                        agent2_motion.generate_trajectory(True)
                        if len(agent2_motion.all_trajectories) == 0 or max([len(x) for x in agent2_motion.all_trajectories.values()]) == 0:
                            continue
                        all_trajs = []
                        for k,v in agent2_motion.all_trajectories.items():
                            #print('generated',len(v),k,'trajectories for',ag2_obj_manv)
                            if len(v) == 0:
                                continue
                            '''
                            for traj in v:
                                ax.plot([float(x[0]) for x in traj],[float(x[3])*3.6 for x in traj],'-',color='red' if ag2_obj_manv == 'track_speed' else 'blue')
                                ax.plot([float(x[6])-start_ts for x in scene_def.all_agent_trajectories[1]],[float(x[3]) for x in scene_def.all_agent_trajectories[1]],'-',color='green')
                                print(float(traj[-1][3])*3.6)
                            '''
                            _this_trajs = [[_ for _ in x if _[0] <= scene_def.horizon] for x in v]
                            all_trajs += _this_trajs
                            _err = [LineString([(x[1],x[2]) for x in _t]).length - emp_traj_errs[1][2] for _t in _this_trajs] 
                            min_abs_err = min([abs(x) for x in _err])
                            if min_abs_err < abs(emp_traj_errs[1][1]):
                                emp_traj_errs[1] = (ag2_obj_manv,min_abs_err,emp_traj_errs[1][2])
                        _errs = sorted([LineString([(t[1],t[2]) for t in x]).length-emp_traj_errs[1][2] for x in all_trajs])
                        _length_sorted = sorted(all_trajs, key=lambda x: LineString([(t[1],t[2]) for t in x]).length)
                        all_trajs = sorted(all_trajs, key=lambda x: LineString([(t[1],t[2]) for t in x]).length-emp_traj_errs[1][2])
                        if ag2_obj_manv not in traj_dict[agent_2_id]:
                            lb_idx,p_idx,ub_idx = np.argmin(_errs),np.argmin([abs(x) for x in _errs]),np.argmax(_errs)
                            traj_lb, traj_p, traj_ub = _length_sorted[0],all_trajs[p_idx],_length_sorted[-1]
                            trajl_lb, trajl_p, trajl_ub = LineString([(x[1],x[2]) for x in traj_lb]).length, LineString([(x[1],x[2]) for x in traj_p]).length, LineString([(x[1],x[2]) for x in traj_ub]).length
                            traj_dict[agent_2_id][ag2_obj_manv] = ([traj_lb, traj_p, traj_ub],[trajl_lb, trajl_p, trajl_ub])
                except Exception:
                    continue
                ''' reconcile prototype and empirical trajectories'''
                for agid,v in traj_dict.items():
                    for agmanv,_entry in v.items():
                        agidx = all_agents.index(agid)
                        if agmanv == emp_traj_errs[agidx][0]:
                            _l2match = l2key_map[np.argmin([abs(x-emp_traj_errs[agidx][2]) for x in _entry[1]])]
                            emp_traj_errs[agidx] = (emp_traj_errs[agidx][0]+'-'+_l2match, emp_traj_errs[agidx][1], emp_traj_errs[agidx][2])
                            
                            
                    
                hierar_game_dict = dict()
                if len(scene_def.agent1_emp_traj) == 0 or len(scene_def.agent2_emp_traj) == 0:
                    continue
                #plt.show()
                _args = tuple([list(traj_dict[x].keys()) for x in all_agents])
                l1_keys = list(itertools.product(*[v for v in [list(traj_dict[x].keys()) for x in all_agents]]))
                hierar_game_dict = {str(x):dict() for x in l1_keys}
                
                l2_modifiers = list(itertools.product(*[['lb','p','ub'] for x in np.arange(len(all_agents))]))
                util_object = Utilities()
                for k in l1_keys:
                    l2_keys = [tuple([k[agidx]+'-'+m[agidx] for agidx in np.arange(len(all_agents))]) for m in l2_modifiers]
                    l2_game = {str(x):[] for x in l2_keys}
                    for l2manv in l2_keys:
                        traj_act_comb = []
                        for agidx,agid in enumerate(all_agents):
                            _ag_l1_act, _ag_l2_act = '-'.join(l2manv[agidx].split('-')[0:-1]), l2tag_map[l2manv[agidx].split('-')[-1]]
                            traj_act_comb.append(traj_dict[agid][_ag_l1_act][0][_ag_l2_act])
                        dist_gap = util_object.calc_dist_gap(traj_act_comb[0], traj_act_comb[1], (1,2))
                        traj_length_ag1, traj_length_ag2 = LineString([(x[1],x[2]) for x in traj_act_comb[0]]).length, LineString([(x[1],x[2]) for x in traj_act_comb[1]]).length
                        traj_horz_ag1,traj_horz_ag2 = len(traj_act_comb[0]), len(traj_act_comb[1])
                        traj_length_ag1, traj_length_ag2 = traj_length_ag1 * (60/traj_horz_ag1), traj_length_ag2 * (60/traj_horz_ag2)
                        # Scale it to 6secs
                        prog_util_ag1, prog_util_ag2 = util_object.progress_payoff_dist(traj_length_ag1,'agent_1'), util_object.progress_payoff_dist(traj_length_ag2,'agent_2')
                        l2_game[str(l2manv)] = [[util_object.gen_dist_utils(dist_gap,9),prog_util_ag1],[util_object.gen_dist_utils(dist_gap,5),prog_util_ag2]]
                    hierar_game_dict[str(k)] = l2_game
                with open(os.path.join(cache_path,'_'.join([file_id,str(agent_1_id),str(agent_2_id),str(start_ts)])+'.json'), 'w') as fp:
                    json.dump(hierar_game_dict, fp)
                
                with open(os.path.join('D:\\oneshot_games_data','intersection_dataset','runner_out.csv'), mode='a') as scene_file:
                    sc_writer = csv.writer(scene_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    sc_writer.writerow(row+[emp_traj_errs[0][0],emp_traj_errs[0][1],emp_traj_errs[1][0],emp_traj_errs[1][1]])
                
                print('Generating',line_count,row,emp_traj_errs[0][0],emp_traj_errs[0][1],emp_traj_errs[1][0],emp_traj_errs[1][1]) 
                #plt.show()
                #analytics_obj = UniWeberAnalytics('769')
                #analytics_obj.animate_scene([[(x[1],x[2]) for x in ag1_traj],[(x[1],x[2]) for x in ag2_traj]],None)
                
                f=1                        
                        
  
class CrosswalkGameGenerator():
    
    def setup_maneuver_constraints(self,):
        maneuver_constraints = {'agent_1':{'maneuvers':{'wait':None,'turn':None}, 'agent_state':self.agent1},'agent_2':{'maneuvers':{'ped_walk':None,'ped_wait':None}, 'agent_state':self.agent2}}
        agent2_traj_constr = PedestrianManeuverConstraints(init_vel=self.agent2.velocity,waypoints=self.agent2.waypoints)
        maneuver_constraints['agent_2']['maneuvers']['ped_walk'] = agent2_traj_constr
        maneuver_constraints['agent_2']['maneuvers']['ped_wait'] = agent2_traj_constr
    
    def plot_tracks(self,ax):
        '''
        file_id = '591'
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+'591'+'.db'))
        c = conn.cursor()
        q_string = "select * FROM TRAFFIC_REGIONS_DEF WHERE TRAFFIC_REGIONS_DEF.SHAPE = 'polygon';"
        vehicle_list = []
        c.execute(q_string)
        res = c.fetchall()
        '''
        for reg,row in ss_constants.regions_fixed.items():
            #if row[0] not in ["west_exit","west_entry","e_w_seg","w_e_seg","east_entry","east_exit"]:
            #X,Y = ast.literal_eval(row[4]), ast.literal_eval(row[5])
            X,Y = row[0], row[1]
            X = X + [X[0]]
            Y = Y + [Y[0]]
            ax.plot(X,Y,color="blue",alpha=0.5)
        
    def plot_vels(self):
        file_id = '591'
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+'591'+'.db'))
        c = conn.cursor()
        q_string = "SELECT tan_acc FROM TRAJECTORIES WHERE TRACK_ID IN (select TRACK_ID from TRAJECTORY_MOVEMENTS WHERE TYPE='Pedestrian')"
        c.execute(q_string)
        res = c.fetchall()
        plt.hist([x[0] for x in res], density=True, bins=30) 
        plt.show()
        
    def generate_game_defs(self):
        #self.plot_vels()
        ss_constants.RUNNING_DATASET = 'crosswalk'
        all_games_list = []
        for file_id in [str(x) for x in ss_constants.CROSSWALK_ALL_FILE_IDS]:
            print(file_id)
            conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+file_id+'.db'))
            c = conn.cursor()
            q_string = "SELECT * FROM v_trajectories AS A INNER JOIN ( \
                    select * from RELEVANT_AGENTS ORDER BY TIME,TRACK_ID) AS B ON A.TRACK_ID=B.TRACK_ID AND A.TIME=B.TIME where a.assigned_segment like '%entry%' order by a.time,a.track_id;"
            vehicle_list = []
            c.execute(q_string)
            res = c.fetchall()
            for row in res:
                time_ts, track_id = row[6], row[0]
                relev_agents, rel_type = row[13], row[14]
                if rel_type == 'leading_vehicle':
                    continue
                if track_id not in [x[0] for x in vehicle_list]:
                    # Since the query is ordered by time, the first time instance will be selected just like how we want it.
                    vehicle_list.append((track_id,time_ts,relev_agents))
                else:
                    last_time = max([x[1] for x in vehicle_list if x[0]==track_id])
                    if time_ts-last_time > 6:
                        vehicle_list.append((track_id,time_ts,relev_agents))
            for v in vehicle_list:
                track_id,time_ts,relev_agents = v[0], v[1], v[2]
                cache_path = os.path.join('D:\\oneshot_games_data','crosswalk_dataset','game_trees')
                file_path = os.path.join(cache_path,'_'.join([str(x) for x in [file_id,track_id,time_ts]])+'.json')
                if os.path.isfile(os.path.join(cache_path,'_'.join([str(x) for x in [file_id,track_id,time_ts]])+'.json')):
                    print('row',row,'processed...continuing')
                    continue
                try:
                    
                    if int(file_id) == 603 and time_ts==209.876333:
                        f=1
                    game_def = CrosswalkGameDefinition(file_id=file_id, time_ts=time_ts, veh_id=track_id)
                    game_def.cache_path = cache_path
                    game_def.file_path = file_path
                    game_def.generate_actions()
                    game_def.create_game_tree()
                    game_def.print_game_tree_to_file()
                except InvalidScenarioException as e:
                    print('Invalid Scenario '+str(e),file_id,track_id,time_ts,'continuing....')
                '''
                fig, ax = plt.subplots(1,1)
                self.plot_tracks(ax)
                game_def.plot_generated_trajectories(ax)
                plt.axis('equal')
                plt.show()
                '''
                f=1    
                    
            
            all_games_list += [(file_id,x[0],x[1]) for x in vehicle_list]
            
            
            # Now get the pedestrian that eventually crosses the street next.
            
            
        for x in all_games_list:
            print(x)
        print('count', len(all_games_list))
        
        
class RoundaboutGameGenerator():
    
    def parse_roundabout_map(self,ax):
        patches = []
        way_map, lane_map = dict(), dict()
        #myProj = Proj("+proj=utm +zone=18T, +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
        #root = ET.parse('D:\\repeated_games_data\\intersection_dataset\\maps\\nyc_lanes.osm').getroot()
        myProj = Proj("+proj=utm +zone=17T, +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
        root = ET.parse('..\\dataset_tools\\mapfiles\\roundabout_ll3.osm').getroot()
        node_map = dict()
        for type_tag in root.findall('node'):
            if type_tag.attrib['id'] not in node_map:
                node_map[type_tag.attrib['id']] = (type_tag.attrib['lat'],type_tag.attrib['lon'])
            '''
            node_tags = type_tag.findall('tag')
            if len(node_tags) > 0:
                lat,lon = float(type_tag.attrib['lat']), float(type_tag.attrib['lon']) 
                utm_proj = myProj(lon, lat)
                if node_tags[0].attrib['k'] == 'name':
                    print(node_tags[0].attrib['v'],utm_proj)
            '''
        for type_tag in root.findall('way'):
            if type_tag.find('tag') is not None:
                for way_id in type_tag.find('tag').attrib['v'].split(','):
                    #way_id = type_tag.attrib['id']
                    #print('----',lane_tag)
                    for nd in type_tag:
                        if 'ref' in nd.attrib:
                            lat,lon = float(node_map[nd.attrib['ref']][0]), float(node_map[nd.attrib['ref']][1]) 
                            utm_proj = myProj(lon, lat)
                            
                            #print(utm_proj)
                            if way_id not in way_map:
                                way_map[way_id] = [utm_proj]
                            else:
                                way_map[way_id].append(utm_proj)
        for k,v in way_map.items():
            way_region, way_orientation = k.split('_')[:-1],k.split('_')[-1]
            way_other_orientation = 'left' if way_orientation == 'right' else 'right'
            other_lane_tag = '_'.join(way_region+[way_other_orientation])
            if other_lane_tag in way_map:
                if '_'.join(way_region) not in lane_map:
                    or1, or2 = way_orientation+'_boundary', way_other_orientation+'_boundary', 
                    lane_map['_'.join(way_region)] = {or1:v, or2:way_map['_'.join(way_region+[way_other_orientation])]}
            else:
                print(k)
            #print(k,v)
            #plt.plot([float(x[0]) for x in  v],[float(x[1]) for x in  v],'--',c='black')
        for k,v in lane_map.items():
            print(k)
            for k1,v1 in v.items():
                print('\t',k1,v1)
            lane_region = np.array([list(x) for x in v['left_boundary']+v['right_boundary'][::-1]])
            pl = MPLPolygon(xy=lane_region, closed = False)   
            centr = Polygon([list(x) for x in v['left_boundary']+v['right_boundary'][::-1]]).centroid
            '''
            if ax is not None:
                ax.annotate(k, (centr.x, centr.y), color='black', fontsize=6, ha='center', va='center')
            '''
            patches.append(pl)
        return patches
    
    
    def plot_tracks(self,ax):
        patches = self.parse_roundabout_map(ax)
        p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4)
        colors = 100*np.random.rand(len(patches))
        p.set_array(np.array(colors))
        ax.add_collection(p)
    
    def generate_veh_veh_game_defs(self):
        #self.plot_vels()
        ss_constants.RUNNING_DATASET = 'roundabout'
        all_games_list = []
        for file_id in [str(x) for x in ss_constants.ROUNDABOUT_ALL_FILE_IDS]:
            print(file_id)
            conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
            c = conn.cursor()
            q_string = "SELECT * FROM TRAJECTORIES AS A INNER JOIN \
                            (select * from RELEVANT_AGENTS ORDER BY TIME,TRACK_ID) AS B ON A.TRACK_ID=B.TRACK_ID AND A.TIME=B.TIME where a.traffic_regions like '%feeder2%' order by a.time,a.track_id;"
            vehicle_list = []
            c.execute(q_string)
            res = c.fetchall()
            all_vehicles = [(x[1],x[7]) for x in res]
            for row in res:
                time_ts, track_id = row[7], row[1]
                relev_agents, rel_type = row[12], row[13]
                if rel_type == 'leading_vehicle':
                    if (int(relev_agents),time_ts) in all_vehicles:
                        continue
                if track_id not in [x[0] for x in vehicle_list]:
                    # Since the query is ordered by time, the first time instance will be selected just like how we want it.
                    vehicle_list.append((track_id,time_ts,relev_agents))
                else:
                    last_time = max([x[1] for x in vehicle_list if x[0]==track_id])
                    if time_ts-last_time > 6:
                        vehicle_list.append((track_id,time_ts,relev_agents))
            
            for v in vehicle_list:
                track_id,time_ts,relev_agents = v[0], v[1], v[2]
                cache_path = os.path.join('D:\\oneshot_games_data','roundabout_dataset','game_trees')
                file_path = os.path.join(cache_path,'_'.join([str(x) for x in [file_id,track_id,time_ts]])+'.json')
                if os.path.isfile(os.path.join(cache_path,'_'.join([str(x) for x in [file_id,track_id,time_ts]])+'.json')):
                    print('row',row,'processed...continuing')
                    continue
                try:
                    print(file_path)
                    game_def = RoundaboutGameDefinition(file_id=file_id, time_ts=time_ts, veh_id=track_id)
                    game_def.cache_path = cache_path
                    game_def.file_path = file_path
                    game_def.generate_actions()
                    game_def.create_game_tree()
                    game_def.print_game_tree_to_file()
                except InvalidScenarioException as e:
                    print('Invalid Scenario '+str(e),file_id,track_id,time_ts,'continuing....')
                except InvalidCenterlineException as e:
                    print('Invalid Scenario '+str(e),file_id,track_id,time_ts,'continuing....')
                '''
                fig, ax = plt.subplots(1,1)
                self.plot_tracks(ax)
                game_def.plot_generated_trajectories(ax)
                plt.axis('equal')
                plt.show()
                '''
                f=1    
                    
            
            all_games_list += [(file_id,x[0],x[1]) for x in vehicle_list]
            
            
            # Now get the pedestrian that eventually crosses the street next.
            
            
        for x in all_games_list:
            if int(x[0]) == 917:
                print(x)
        print('count', len(all_games_list))
            
        
def gen_traj_errors():
    rows = [['dataset','agent','error']]
    ss_constants.RUNNING_DATASET = 'crosswalk'
    csv_path = os.path.join(ss_constants.dataset_home,ss_constants.RUNNING_DATASET+'_dataset','runner_out.csv') 
    with open(csv_path, mode='r',newline='\n') as tax_map_file:
        sc_reader = csv.reader(tax_map_file, delimiter=',')
        for row in sc_reader:
            file_id = row[0]
            time_ts = row[1]
            agents = row[2].split(",")
            ag_key = ss_utils.get_agent_key(agents)
            ag_errs = [float(x) for x in row[4].split(',')]
            rows.append([ss_constants.RUNNING_DATASET,ag_key[0],ag_errs[0]])
            rows.append([ss_constants.RUNNING_DATASET,'other',np.mean([abs(x) for x in ag_errs[1:]])])
            print(ss_constants.RUNNING_DATASET,file_id)
    
    ss_constants.RUNNING_DATASET = 'intersection'
    csv_path = os.path.join(ss_constants.dataset_home,ss_constants.RUNNING_DATASET+'_dataset','runner_out.csv') 
    with open(csv_path, mode='r',newline='\n') as tax_map_file:
        sc_reader = csv.reader(tax_map_file, delimiter=',')
        for row in sc_reader:
            file_id = row[0]
            ag1,ag2,time_ts = row[3], row[4], row[5] if ast.literal_eval(row[5]) != 0 else '0.0'
            file_key = '_'.join([file_id,ag1,ag2,time_ts])
            ag_key = ss_utils.get_agent_key([ag1,ag2])
            ag_errs = [float(row[7]),float(row[9])]
            for agidx in np.arange(len(ag_key)):
                rows.append([ss_constants.RUNNING_DATASET,ag_key[agidx],ag_errs[agidx]])
            print(ss_constants.RUNNING_DATASET,file_id)
    
    ss_constants.RUNNING_DATASET = 'roundabout'
    csv_path = os.path.join(ss_constants.dataset_home,ss_constants.RUNNING_DATASET+'_dataset','runner_out.csv') 
    with open(csv_path, mode='r',newline='\n') as tax_map_file:
        sc_reader = csv.reader(tax_map_file, delimiter=',')
        for row in sc_reader:
            
            file_id = row[0]
            time_ts = row[1]
            agents = row[2].split(",")
            ag_key = ss_utils.get_agent_key(agents)
            ag_errs = [float(x) for x in row[4].split(',')]
            rows.append([ss_constants.RUNNING_DATASET,ag_key[0],ag_errs[0]])
            rows.append([ss_constants.RUNNING_DATASET,'other',np.mean([abs(x) for x in ag_errs[1:]])])
            print(ss_constants.RUNNING_DATASET,file_id)
            
    csv_path = os.path.join(ss_constants.dataset_home,'trajectory_errs.csv') 
    with open(csv_path, mode='w') as pref_file:
        sc_writer = csv.writer(pref_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in rows:
            sc_writer.writerow(row)

if __name__ == '__main__':
    #unittest.main()
    gen_traj_errors()
    '''
    rg_constants.DATASET = 'intersection_dataset'
    rg_constants.SCENE_TYPE = ('REAL','intersection_dataset')
    scenario = GameGenerator()
    scenario.generate()
    '''
    '''
    generator = CrosswalkGameGenerator()
    generator.generate_game_defs()
    '''
    '''
    generator = RoundaboutGameGenerator()
    generator.generate_veh_veh_game_defs()
    '''