'''
Created on Dec 7, 2021

@author: Atrisha
'''
from equilibria.equilibria_core import EquilibriaCore as HGEqCore
import numpy as np
import operator
import itertools
from subprocess import check_output, CalledProcessError
import os
import ss_utils
import ast
import ss_constants

class EquilibriaCore(HGEqCore):
    
    def keywithmaxval(self,d):
        """ a) create a list of the dict's keys and values; 
            b) return the key with the max value"""  
        v=np.asarray(list(d.values()))
        k=list(d.keys())
        max_v = max(v)
        indices = np.where(v == max_v)[0]
        return [k[x] for x in indices],max_v
    
    def calc_common_interest_degree(self,nash_resps):
        opt_strat = []
        payoff_dict = self.pay_off_dict
        for agidx in np.arange(self.num_players):
            this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items()}
            ag_opt_strat,opt_val = self.keywithmaxval(this_player_payoff_dict)
            opt_strat.append(ag_opt_strat)
        ct = 0
        for agidx in np.arange(self.num_players):
            if any(x in nash_resps for x in opt_strat[agidx]):
                ct += 1
        deg_ci = ct / self.num_players
        return deg_ci
                
    
    def calc_max_max_response(self,all=True):
        opt_strat = []
        payoff_dict = self.pay_off_dict
        for agidx in np.arange(self.num_players):
            this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items()}
            ag_opt_strat,opt_val = self.keywithmaxval(this_player_payoff_dict)
            ag_opt_strat = list(set([x[agidx] for x in ag_opt_strat]))
            opt_strat.append(ag_opt_strat)
        maxmax_strats = list(itertools.product(*opt_strat))
        return maxmax_strats
    
    def calc_max_min_response(self):
        payoff_dict = self.pay_off_dict
        num_players = self.num_players
        res_dict = {n:{} for n in np.arange(self.num_players)}
        for s,p in payoff_dict.items():
            for i in np.arange(num_players):  
                # Worst that can happen for each player
                if s[i] not in res_dict[i]:
                    res_dict[i][s[i]] = np.inf
                if p[i] < res_dict[i][s[i]]:
                    res_dict[i][s[i]] = p[i]
        opt_strat = []
        self.sv_action_payoffs = []
        for agidx in np.arange(num_players):
            max_val = max(res_dict[agidx].values())
            ag_opt_strat = [_s for _s,_p in res_dict[agidx].items() if _p == max_val]
            opt_strat.append(ag_opt_strat)
        maxmin_strats = list(itertools.product(*opt_strat))
        return maxmin_strats
    
    def calc_pure_strategy_nash_equilibrium_gambit(self,file_str,all_agents):
        nfg_file_str = file_str+'.nfg'
        self.transform_to_nfg_format(nfg_file_str,all_agents)
        try:
            if len(self.pay_off_dict) >= 243:
                out = check_output('"C:\\Program Files (x86)\\Gambit\\gambit-gnm.exe" -d 1 -q "'+nfg_file_str+'"', shell=True).decode()
            else:
                out = check_output('"C:\\Program Files (x86)\\Gambit\\gambit-enumpure.exe" -q "'+nfg_file_str+'"', shell=True).decode()
            outfile_loc = file_str+'.ne'
            returncode = 0
        except CalledProcessError:
            returncode = 1
        if returncode == 0:
            text_file = open(outfile_loc, "w")
            text_file.write(out)
            text_file.close()
        return returncode
            
    def read_gambit_ne(self,file_str, all_acts):
        res_f = file_str+'.ne'
        ne_list = []
        with open(res_f) as f:
            ne_res_high_bounds = f.readlines()
            for l in ne_res_high_bounds:
                code_str = l.rstrip().split(',')[1:]
                if len(code_str) != len(all_acts):
                    continue
                ne_tuple = tuple([x for i,x in enumerate(all_acts) if ast.literal_eval(code_str[i]) == 1])
                if ne_tuple not in ne_list:
                    ne_list.append(ne_tuple)
        f=1
        opt_strat = ne_list
        opt_strat = list(set(opt_strat))
        return opt_strat
    
    
    def calc_stackelberg_response(self):
        payoff_dict = self.pay_off_dict
        leader_actions = list(set([k[0] for k in payoff_dict.keys()]))
        stackelberg_responses = dict()
        for l_act in leader_actions:
            follower_resp_options = {k:v[1] for k,v in payoff_dict.items() if k[0]==l_act}
            f_opt_strat,opt_val = self.keywithmaxval(follower_resp_options)
            f_opt_strat = list(set([x[1] for x in f_opt_strat]))
            stackelberg_responses.update({k:v for k,v in payoff_dict.items() if k[0]==l_act and k[1] in f_opt_strat})
        f_payoff_dict = {k:v[0] for k,v in stackelberg_responses.items()}
        stack_opt_strats,opt_val = self.keywithmaxval(f_payoff_dict)   
        return stack_opt_strats
    
    def calc_level1_response(self, model_config):
        payoff_dict = self.pay_off_dict
        if model_config.l2_soln == 'maxmax':
            l0_resp = self.calc_max_max_response()
        elif model_config.l2_soln == 'maxmin':
            l0_resp = self.calc_max_min_response()
        else:
            raise Exception('unsupported l2 solution concept')
        opt_strat = []
        for agidx in np.arange(self.num_players):
            oth_agent_l0resp = [tuple([act for idx,act in enumerate(list(x)) if idx!=agidx]) for x in l0_resp]
            this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items() if tuple([act for idx,act in enumerate(list(k)) if idx!=agidx]) in oth_agent_l0resp}
            ag_opt_strat,opt_val = self.keywithmaxval(this_player_payoff_dict)
            ag_opt_strat = list(set([x[agidx] for x in ag_opt_strat]))
            opt_strat.append(ag_opt_strat)
        l1_strats = list(itertools.product(*opt_strat))
        return l1_strats
    
    def calc_level2_response(self, model_config):
        payoff_dict = self.pay_off_dict
        l1_resp = self.calc_level1_response(model_config)
        opt_strat = []
        for agidx in np.arange(self.num_players):
            oth_agent_l1resp = [tuple([act for idx,act in enumerate(list(x)) if idx!=agidx]) for x in l1_resp]
            this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items() if tuple([act for idx,act in enumerate(list(k)) if idx!=agidx]) in oth_agent_l1resp}
            ag_opt_strat,opt_val = self.keywithmaxval(this_player_payoff_dict)
            ag_opt_strat = list(set([x[agidx] for x in ag_opt_strat]))
            opt_strat.append(ag_opt_strat)
        l2_strats = list(itertools.product(*opt_strat))
        return l2_strats
    
    def calc_level1R_response(self, model_config):
        payoff_dict = self.pay_off_dict
        all_agents = [model_config.gamefile.split('_')[1]] + ['-'.join(x.split('-')[:-1]) for x in list(next(iter(payoff_dict)))[1:]]
        l0_resp = [ss_utils.get_rule_strat(all_agents)]
        opt_strat = []
        for agidx in np.arange(self.num_players):
            oth_agent_l0resp = [tuple([act for idx,act in enumerate(list(x)) if idx!=agidx]) for x in l0_resp]
            if ss_constants.RUNNING_DATASET != 'intersection':
                this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items() if tuple([act if idx==0 else act.split('-')[-1] for idx,act in enumerate(list(k)) if idx!=agidx]) in oth_agent_l0resp}
            else:
                this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items() if tuple([act for idx,act in enumerate(list(k)) if idx!=agidx]) in oth_agent_l0resp}
            if len(this_player_payoff_dict) == 0:
                return []
            ag_opt_strat,opt_val = self.keywithmaxval(this_player_payoff_dict)
            ag_opt_strat = list(set([x[agidx] for x in ag_opt_strat]))
            opt_strat.append(ag_opt_strat)
        l1_strats = list(itertools.product(*opt_strat))
        return l1_strats
        
            
            
class CommonInterestAnalysis():
    
    def calc_max_min_response(self,payoff_dict,num_players):
        res_dict = {n:{} for n in np.arange(num_players)}
        for s,p in payoff_dict.items():
            for i in np.arange(num_players):  
                # Worst that can happen for each player
                if s[i] not in res_dict[i]:
                    res_dict[i][s[i]] = np.inf
                if p[i] < res_dict[i][s[i]]:
                    res_dict[i][s[i]] = p[i]
        opt_strat = []
        self.sv_action_payoffs = []
        for agidx in np.arange(num_players):
            max_val = max(res_dict[agidx].values())
            ag_opt_strat = [_s for _s,_p in res_dict[agidx].items() if _p == max_val]
            opt_strat.append(ag_opt_strat)
        return opt_strat
    
    def __init__(self,maxmax_resps, nash_resps, all_agents, obs_strat, payoff_dict):
        maxmax_resps = [list(set([x[agidx] for x in maxmax_resps])) for agidx in np.arange(len(all_agents))]
        assert len(maxmax_resps) == len(all_agents)
        assert len(obs_strat) == len(all_agents)
        self.payoff_dict = payoff_dict
        self.maxmax_resps = maxmax_resps
        self.nash_resps = nash_resps
        self.all_agents = all_agents
        self.obs_strat = obs_strat
        self.common_interest_ctr = [0]*len(all_agents)
        self.payoff_dominant_ctr = [0]*len(all_agents)
        self.risk_dominant_ctr = [0]*len(all_agents)
        
    def does_match(self,obs_strat,list_of_strats,agidx):
        act_only_resps = []
        if ss_constants.RUNNING_DATASET == 'intersection':
            act_only_resps = list_of_strats
        else:
            for x in list_of_strats:
                if agidx == 0:
                    act_only_resps.append(x)
                else:
                    act_only_resps.append(x.split('-')[-1])
        
        if obs_strat[agidx] in act_only_resps:
            return True
        else:
            return False
        
        
    def assign_risk_and_payoff_dominance(self):
        for agidx in np.arange(len(self.all_agents)):
            if len(list(set(self.maxmax_resps[agidx]) & set([x[agidx] for x in self.nash_resps]))) > 0 :
                if self.does_match(self.obs_strat, self.maxmax_resps[agidx], agidx):
                    self.payoff_dominant_ctr[agidx] += 1
                self.common_interest_ctr[agidx] += 1
        
        princip_agent_nash_acts = list(set([x[0] for x in self.nash_resps]))
        for agidx in np.arange(len(self.all_agents)):
            if self.common_interest_ctr[agidx]==1 and self.payoff_dominant_ctr[agidx] == 0:
                if agidx == 0:
                    nash_act_closure = list(itertools.product(*[list(set([x[agidx] for x in self.nash_resps])) for agidx in np.arange(len(self.all_agents))]))
                    ci_only_payoff = dict()
                    for strat,payoff in self.payoff_dict.items():
                        if strat in nash_act_closure:
                            _ci_ag_strat = tuple([strat[idx] for idx,_s in enumerate(list(strat)) if self.common_interest_ctr[idx] != 0])
                            _ci_ag_payoff = [payoff[idx] for idx,_p in enumerate(list(payoff)) if self.common_interest_ctr[idx] != 0]
                            ci_only_payoff[_ci_ag_strat] = _ci_ag_payoff
                    risk_dominant_strats = self.calc_max_min_response(ci_only_payoff, len([x for x in self.common_interest_ctr if x != 0]))
                    if len(risk_dominant_strats) > 0:
                        princip_agent_risk_dom_strat = risk_dominant_strats[0]
                        if self.does_match(self.obs_strat, princip_agent_risk_dom_strat, agidx):
                            self.risk_dominant_ctr[agidx] += 1
                else:
                    # Create 2x2 games with principal agent
                    ci_only_payoff = dict()
                    for strat,payoff in self.payoff_dict.items():
                        if strat[0] in princip_agent_nash_acts:
                            if (strat[0],strat[agidx]) not in ci_only_payoff:
                                ci_only_payoff[(strat[0],strat[agidx])] = [[],[]]
                            ci_only_payoff[(strat[0],strat[agidx])][0].append(payoff[0])
                            ci_only_payoff[(strat[0],strat[agidx])][1].append(payoff[agidx])
                    # Payoffs may be dependent on latent third agent action, so aggregate the payoffs based on max. Any other aggregation
                    # such as mean can also be used.
                    ci_only_payoff = {k:[max(v[0]),max(v[1])] for k,v in ci_only_payoff.items()}
                    risk_dominant_strats = self.calc_max_min_response(ci_only_payoff, 2)
                    if len(risk_dominant_strats) > 1:
                        agent_risk_dom_strat = risk_dominant_strats[1]
                        if self.does_match(self.obs_strat, agent_risk_dom_strat, agidx):
                            self.risk_dominant_ctr[agidx] += 1
        
                
