'''
Created on Dec 7, 2021

@author: Atrisha
'''
import ss_constants
import ast
import numpy as np

class PreferenceConfig():
    
    def __init__(self, preference_type, prediction_model):
        self.prediction_model = prediction_model
        self.preference_type = preference_type
        
    
    def convert_gametree_prefs(self,gametree,ponly_run=False):
        # Higher satis_val is low risk tolerance
        converted_gametree = dict()
        for l1_manv,l1_tree in gametree.items():
            conv_l2tree = dict()
            for l2_manv, utils in l1_tree.items():
                l2_manv_key = ast.literal_eval(l2_manv)
                if ponly_run:
                    if not all([x=='p' for x in [x.split('-')[-1] for x in l2_manv_key]]):
                        continue
                predicted_params = []
                if self.prediction_model is not None:
                    for agidx in np.arange(len(utils)):
                        ag_key = self.ag_keys[agidx]
                        X = np.asarray([utils[agidx][0],self.ag_vels[agidx]])
                        X = X.reshape(1,2)
                        if ag_key in self.prediction_model and not np.isnan(self.ag_vels[agidx]):
                            predicted_param =  self.prediction_model[ag_key].predict(X)
                            predicted_params.append(predicted_param[0])
                        else:
                            if self.preference_type == 'satisfied':
                                predicted_params.append(0)
                            else:
                                predicted_params.append(0.5)
                else:
                    predicted_params = [0]*len(utils) if self.preference_type == 'satisfied' else [0.5]*len(utils)
                if self.preference_type == 'satisfied':
                    conv_utils = [x[1] if x[0] >= predicted_params[idx] else x[0] for idx,x in enumerate(utils)]
                else:
                    
                    conv_utils = [(x[0]*predicted_params[idx])+((1-predicted_params[idx])*x[1]) for idx,x in enumerate(utils)]
                conv_l2tree[l2_manv_key] = conv_utils
            converted_gametree[ast.literal_eval(l1_manv)] = conv_l2tree
        return converted_gametree 

    
class ModelConfig():
    
    def __init__(self,running_dataset):
        self.running_dataset = running_dataset
        
