'''
Created on Dec 7, 2021

@author: Atrisha
'''
import os
import ss_constants
import ujson as json
from models.config_classes import PreferenceConfig, ModelConfig
from models.solution_classes import EquilibriaCore, CommonInterestAnalysis
from models.preference_estimation import SatisficingPreferenceEstimation, interval, WeightedPreferenceEstimation, PreferenceEstimation
from models.preference_estimation import PredictiveModel
import ast
import numpy as np
from scipy.optimize import curve_fit
import scipy.stats as st
import csv
import itertools
from ss_utils import GametreeUtils
import shutil
import ss_utils
import matplotlib.pyplot as plt


def keywithmaxval(d):
        """ a) create a list of the dict's keys and values; 
            b) return the key with the max value"""  
        v=np.asarray(list(d.values()))
        k=list(d.keys())
        max_v = max(v)
        indices = np.where(v == max_v)[0]
        return [k[x] for x in indices],max_v

def is_common_interest_game(payoff_dict,num_players):
    opt_strat = []
    for agidx in np.arange(num_players):
        this_player_payoff_dict = {k:v[agidx] for k,v in payoff_dict.items()}
        ag_opt_strat,opt_val = keywithmaxval(this_player_payoff_dict)
        opt_strat.append(ag_opt_strat)
    n_common_interest_strats =  len(list(set.intersection(*map(set,opt_strat))))
    if n_common_interest_strats > 0:
        if max([len(x) for x in opt_strat]) == min([len(x) for x in opt_strat]) & max([len(x) for x in opt_strat]) == n_common_interest_strats:
            return 'strong'
        else:
            return 'weak'
    else:
        return None

def clear_cache():
    dataset_home = 'D:\\oneshot_games_data'
    cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees','cache')
    for filename in os.listdir(cache_path):
        file_path = os.path.join(cache_path, filename)
        os.unlink(file_path)

def load_emp_acts():
    csv_path = os.path.join(ss_constants.dataset_home,ss_constants.RUNNING_DATASET+'_dataset','runner_out.csv')
    emp_acts = dict()
    if ss_constants.RUNNING_DATASET == 'crosswalk':
        with open(csv_path, mode='r',newline='\n') as tax_map_file:
            sc_reader = csv.reader(tax_map_file, delimiter=',')
            for row in sc_reader:
                file_id = row[0]
                time_ts = row[1]
                agents = row[2].split(",")
                acts = row[3].split(",")
                file_key = '_'.join([file_id,agents[0],time_ts])
                emp_acts[file_key] = {agents[idx]:acts[idx] for idx in np.arange(len(agents))}
    elif ss_constants.RUNNING_DATASET == 'intersection':
        with open(csv_path, mode='r',newline='\n') as tax_map_file:
            sc_reader = csv.reader(tax_map_file, delimiter=',')
            for row in sc_reader:
                file_id = row[0]
                ag1,ag2,time_ts = row[3], row[4], row[5] if ast.literal_eval(row[5]) != 0 else '0.0'
                file_key = '_'.join([file_id,ag1,ag2,time_ts])
                emp_acts[file_key] = {ag1:row[6],ag2:row[8]}
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        with open(csv_path, mode='r',newline='\n') as tax_map_file:
            sc_reader = csv.reader(tax_map_file, delimiter=',')
            for row in sc_reader:
                file_id = row[0]
                time_ts = row[1]
                agents = row[2].split(",")
                acts = row[3].split(",")
                file_key = '_'.join([file_id,agents[0],time_ts])
                emp_acts[file_key] = {agents[idx]:acts[idx] for idx in np.arange(len(agents))}
        
    return emp_acts
    
def solve_l2games(model_config):
    ss_constants.RUNNING_DATASET = model_config.running_dataset
    pref_model_type = model_config.pref_model
    pref_type = model_config.pref_type
    #pref_model_type = 'maxmax'
    l2_game_dir_name = 'l2_solved_games_'+pref_type+'_'+pref_model_type
    dataset_home = ss_constants.dataset_home
    cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
    if not os.path.exists(os.path.join(cache_path,'cache')):
        os.mkdir(os.path.join(cache_path,'cache'))
    if not os.path.exists(os.path.join(cache_path,l2_game_dir_name)):
        os.mkdir(os.path.join(cache_path,l2_game_dir_name))
            
    pref_prediction = PredictiveModel()
    if 'baseline' not in l2_game_dir_name:
        prediction_models = pref_prediction.build(pref_type+'_thresh',pref_model_type) if pref_type == 'satisfied' else pref_prediction.build(pref_type,pref_model_type)
    else:
        prediction_models = None
    pref_config = PreferenceConfig(pref_type,prediction_models)
    gamefiles = [f for f in os.listdir(cache_path) if os.path.isfile(os.path.join(cache_path, f))]
    prog_ctr,n_games = 0,len(gamefiles)
    for gamefile in gamefiles:
        prog_ctr += 1
        
        with open(os.path.join(cache_path, gamefile)) as f:
            try:
                multiobj_gametree = json.load(f)
            except AttributeError:
                continue
            if len(multiobj_gametree) == 0:
                continue
            if ss_constants.RUNNING_DATASET != 'intersection':
                all_agents = [gamefile.split('_')[1]] + ['-'.join(x.split('-')[:-1]) for x in list(ast.literal_eval(next(iter(multiobj_gametree))))[1:]]
            else:
                all_agents = [gamefile.split('_')[1],gamefile.split('_')[2]]
            f=1   
            file_id = gamefile.split('_')[0]
            time_ts = '.'.join(gamefile.split('.')[0:-1]).split('_')[-1]
            ag_vels = [ss_utils.get_agent_velocity(file_id,this_ag,time_ts) for this_ag in all_agents]
            pref_config.ag_vels = ag_vels
            pref_config.ag_keys = ss_utils.get_agent_key(all_agents)
            gametree = pref_config.convert_gametree_prefs(multiobj_gametree,model_config.ponly_run)
            print('processing',prog_ctr,'/',n_games,gamefile,len(gametree))
            if not model_config.ponly_run:
                
                # Solve level2 games with maxmax solution
                maxmax_solved_tree = dict()
                for l1manv, l2tree in gametree.items():
                    eqcore = EquilibriaCore(num_players=len(l1manv),pay_off_dict=l2tree,N=len(l2tree),sv_actions=list(set([x[0] for x in gametree.keys()])),is_l1agent=False)
                    maxmax_solutions = eqcore.calc_max_max_response()
                    # Select the utilatarian welfare maximizing solution
                    l2maxmaxresp = sorted([(k,sum(v)) for k,v in l2tree.items() if k in maxmax_solutions], key=lambda tup: tup[1], reverse=True)[0][0]
                    maxmax_solved_tree[str(l1manv)] = (l2maxmaxresp,l2tree[l2maxmaxresp])
                with open(os.path.join(cache_path,l2_game_dir_name,gamefile+'_l2maxmax.json'), 'w') as fp:
                    json.dump(maxmax_solved_tree, fp)
                 
                 
                # Solve level2 games with maxmin solution
                maxmin_solved_tree = dict()
                for l1manv, l2tree in gametree.items():
                    eqcore = EquilibriaCore(num_players=len(l1manv),pay_off_dict=l2tree,N=len(l2tree),sv_actions=list(set([x[0] for x in gametree.keys()])),is_l1agent=False)
                    maxmin_solutions = eqcore.calc_max_min_response()
                    # Select the utilatarian welfare maximizing solution
                    l2maxminresp = sorted([(k,sum(v)) for k,v in l2tree.items() if k in maxmin_solutions], key=lambda tup: tup[1], reverse=True)[0][0]
                    maxmin_solved_tree[str(l1manv)] = (l2maxminresp,l2tree[l2maxminresp])
                with open(os.path.join(cache_path,l2_game_dir_name,gamefile+'_l2maxmin.json'), 'w') as fp:
                    json.dump(maxmin_solved_tree, fp)
                
                # Solve level2 games with Stackelbeg solution wherever applicable
                if len(next(iter(gametree))) == 2:
                    stackelberg_solved_tree = dict()
                    for l1manv, l2tree in gametree.items():
                        eqcore = EquilibriaCore(num_players=len(l1manv),pay_off_dict=l2tree,N=len(l2tree),sv_actions=list(set([x[0] for x in gametree.keys()])),is_l1agent=False)
                        stackelberg_solutions = eqcore.calc_stackelberg_response()
                        # Select the utilatarian welfare maximizing solution
                        stackelbergresp = sorted([(k,sum(v)) for k,v in l2tree.items() if k in stackelberg_solutions], key=lambda tup: tup[1], reverse=True)[0][0]
                        stackelberg_solved_tree[str(l1manv)] = (stackelbergresp,l2tree[stackelbergresp])
                    if not os.path.exists(os.path.join(cache_path,l2_game_dir_name)):
                        os.mkdir(os.path.join(cache_path,l2_game_dir_name))
                    with open(os.path.join(cache_path,l2_game_dir_name,gamefile+'_l2stackelberg.json'), 'w') as fp:
                        json.dump(stackelberg_solved_tree, fp)
            else:
                ponly_tree = {str(k):(list(v.keys())[0],list(v.values())[0]) for k,v in gametree.items()}
                if not os.path.exists(os.path.join(cache_path,l2_game_dir_name)):
                    os.mkdir(os.path.join(cache_path,l2_game_dir_name))
                with open(os.path.join(cache_path,l2_game_dir_name,gamefile+'_l2ponly.json'), 'w') as fp:
                    json.dump(ponly_tree, fp)

def does_match_resp(emp_acts,responses, all_agents, gamefile, payoff_dict):
    act_only_resps = []
    file_key = '.'.join(gamefile.split('.')[:-2])
    if ss_constants.RUNNING_DATASET == 'intersection':
        act_only_resps = responses
        int_agents = gamefile.split('_')[1:3]
        this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(int_agents)]
        _converted_payoff_dict = payoff_dict
    else:
        for resp in responses:
            _act = [x if idx == 0 else x.split('-')[-1] for idx,x in enumerate(list(resp))]
            act_only_resps.append(tuple(_act))
        this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(all_agents)]
        _converted_payoff_dict = {tuple([x if idx == 0 else x.split('-')[-1] for idx,x in enumerate(k)]):v for k,v in payoff_dict.items()}
    this_game_empact = tuple(['-'.join(x.split('-')[:-1]) for x in this_game_empact])
    match_resp = []
    for agidx,ag in enumerate(all_agents):
        if this_game_empact[agidx] in [x[agidx] for x in act_only_resps]:
            if this_game_empact[agidx] in ss_constants.WAIT_ACTIONS:
                match_resp.append((ss_constants.TRUE_NEGATIVE,0))
            else:
                match_resp.append((ss_constants.TRUE_POSITIVE,0))
        else:
            delta_u = []
            for resp_strat in act_only_resps:
                _emp_replaced_strat = tuple([x if idx!=agidx else this_game_empact[agidx] for idx,x in enumerate(list(resp_strat))])
                if resp_strat not in _converted_payoff_dict or _emp_replaced_strat not in _converted_payoff_dict:
                    # This can happen for rule responses when the strategy was not in the payoff dict due to trajectories not being generated 
                    # in that game state
                    continue
                else:
                    payoff_diff = _converted_payoff_dict[resp_strat][agidx]-_converted_payoff_dict[_emp_replaced_strat][agidx]
                    delta_u.append(payoff_diff)
            if len(delta_u) > 0:
                match_resp.append((ss_constants.FALSE_POSITIVE if this_game_empact[agidx] in ss_constants.WAIT_ACTIONS else ss_constants.FALSE_NEGATIVE,min(delta_u)))
            else:
                match_resp.append((ss_constants.FALSE_POSITIVE if this_game_empact[agidx] in ss_constants.WAIT_ACTIONS else ss_constants.FALSE_NEGATIVE,np.nan))
    return match_resp
    '''
    if this_game_empact in act_only_resps:
        return True
    else:
        return False  
    '''

def plot_emp_act_distribution():
    ss_constants.RUNNING_DATASET = 'intersection'
    dataset_home = ss_constants.dataset_home
    cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
    gamefiles = [f for f in os.listdir(cache_path) if os.path.isfile(os.path.join(cache_path, f))]
    prog_ctr,n_games = 0,len(gamefiles)
    pref_config = PreferenceConfig(None,None)
    emp_acts = load_emp_acts()
    act_distr = dict()
    for file_key,info in emp_acts.items():
        for agidx, fileag_key in enumerate(list(info.keys())): 
            act = info[fileag_key]
            if fileag_key.isdigit():
                this_ag = ast.literal_eval(fileag_key)
            else:
                this_ag = fileag_key
                if ss_constants.RUNNING_DATASET == 'crosswalk':
                    this_ag = this_ag.split('-')[1]
                    if 'ext' in this_ag:
                        this_ag = '_'.join(this_ag.split('_')[:-1])
            if ss_constants.RUNNING_DATASET != 'intersection':
                if isinstance(this_ag, int):
                    ag_type = 'principal'
                else:
                    ag_type = 'segment'
                if ag_type == 'principal':
                    ag_key = ag_type
                else:
                    if ss_constants.RUNNING_DATASET == 'roundabout':
                        ag_key = 'feeder2' if 'feeder2' in this_ag else '_'.join(this_ag.split('_')[1:])
                    else:
                        ag_key = this_ag
            else:
                if agidx == 0:
                    ag_key = 'turning'
                else:
                    ag_key = 'straight-through'
            if ag_key not in act_distr:
                act_distr[ag_key] = dict()
            if act not in act_distr[ag_key]:
                act_distr[ag_key][act] = 1
            else:
                act_distr[ag_key][act] += 1
        print(file_key)
    for k,v in act_distr.items():
        plt.figure()
        plt.title(k)
        x = np.arange(len(v))
        plt.bar(x, height=list(v.values()))
        plt.xticks(x, list(v.keys()))
        plt.show()

def estimate_preference(model_config):
    ss_constants.RUNNING_DATASET = model_config.running_dataset
    pref_type = model_config.pref_type
    pref_model = model_config.pref_model
    recreate = True
    dataset_home = ss_constants.dataset_home
    cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
    gamefiles = [f for f in os.listdir(cache_path) if os.path.isfile(os.path.join(cache_path, f))]
    prog_ctr,n_games = 0,len(gamefiles)
    pref_config = PreferenceConfig(None,None)
    emp_acts = load_emp_acts()
    if pref_type == 'weighted':
        csv_path = os.path.join('D:\\oneshot_games_data',ss_constants.RUNNING_DATASET+'_dataset','weighted_params_'+pref_model+'.csv')
    else:
        csv_path = os.path.join('D:\\oneshot_games_data',ss_constants.RUNNING_DATASET+'_dataset','satisfied_thresh_params_'+pref_model+'.csv')
    if recreate:
        if os.path.isfile(csv_path):
            os.unlink(csv_path)
    print_rows = []
    for gamefile in gamefiles:
        prog_ctr += 1
        with open(os.path.join(cache_path, gamefile)) as f:
            try:
                multiobj_gametree = json.load(f)
            except AttributeError:
                continue
            
            if len(multiobj_gametree) == 0:
                continue
            if ss_constants.RUNNING_DATASET != 'intersection':
                all_agents = [gamefile.split('_')[1]] + ['-'.join(x.split('-')[:-1]) for x in list(ast.literal_eval(next(iter(multiobj_gametree))))[1:]]
            else:
                all_agents = [gamefile.split('_')[1],gamefile.split('_')[2]]
            if pref_type == 'weighted':
                prefs = WeightedPreferenceEstimation(file_str=gamefile,gametree=multiobj_gametree, all_agents = all_agents, emp_acts = emp_acts, game_type='l1')
            else:
                prefs = SatisficingPreferenceEstimation(file_str=gamefile,gametree=multiobj_gametree, all_agents = all_agents, emp_acts = emp_acts, game_type='l1')
            if len(prefs.obs_strat) == 0:
                continue
            prefs.pref_model = pref_model
            thresh_map = prefs.estimate_preference()
            if ss_constants.RUNNING_DATASET == 'intersection':
                print_rows += prefs.build_intersection_param_model()
            else:
                print_rows += prefs.build_param_model()
            print('processing',prog_ctr,'/',n_games,gamefile)
            
    with open(csv_path, mode='a') as pref_file:
        sc_writer = csv.writer(pref_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        sc_writer.writerow(['AGENT','SAFETY','PARAMETER','ACTION','VELOCITY'])
        for row in print_rows:
            sc_writer.writerow(row)
    

def update_match_table(match_table,all_agents,match_list,model):
    match_list_flag,match_list_delta = [x[0] for x in match_list], [x[1] for x in match_list]
    ag_keys = ss_utils.get_agent_key(all_agents)
    if model not in match_table:
        match_table[model] = dict()
    for agidx,ag in enumerate(match_list_flag):
        if ag_keys[agidx] not in match_table[model]:
            match_table[model][ag_keys[agidx]] = []
        match_table[model][ag_keys[agidx]].append((match_list_flag[agidx],match_list_delta[agidx]))
        
            
def update_ci_table(ci_analysis_table,all_agents,ci_analysis_obj):
    ag_keys = ss_utils.get_agent_key(all_agents)
    for agidx,ag in enumerate(ci_analysis_obj.common_interest_ctr):
        if ag_keys[agidx] not in ci_analysis_table:
            ci_analysis_table[ag_keys[agidx]] = []
        ci_analysis_table[ag_keys[agidx]].append([ci_analysis_obj.common_interest_ctr[agidx], ci_analysis_obj.payoff_dominant_ctr[agidx], ci_analysis_obj.risk_dominant_ctr[agidx]])
        

def get_emp_act_for_game( emp_acts, all_agents, gamefile, game_type):   
        act_only_resps = []
        _file_tokens = gamefile.split('.')
        file_key = '.'.join(gamefile.split('.')[:-2]) if game_type =='l2' else '.'.join(gamefile.split('.')[:-1])
        if ss_constants.RUNNING_DATASET == 'intersection':
            int_agents = gamefile.split('_')[1:3]
            this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(int_agents)] if file_key in emp_acts else {}
        else:
            this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(all_agents)] if file_key in emp_acts else {}
        this_game_empact = tuple(['-'.join(x.split('-')[:-1]) for x in this_game_empact])       
        return this_game_empact

def solve_l1games(model_config):
    
    dataset_home = ss_constants.dataset_home
    ss_constants.RUNNING_DATASET = model_config.running_dataset
    pref_model_type = model_config.pref_model
    pref_type = model_config.pref_type
    l2_soln = model_config.l2_soln
    l2_game_dir_name = 'l2_solved_games_'+pref_type+'_'+pref_model_type
    cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees',l2_game_dir_name)
    if not os.path.exists(os.path.join(cache_path,'cache')):
        os.mkdir(os.path.join(cache_path,'cache'))
    else:
        shutil.rmtree(os.path.join(cache_path,'cache'))
        os.mkdir(os.path.join(cache_path,'cache'))
    result_rows = [['agent','pref_type','pref_model_type','l2_soln','l1_soln','tp','tn','fp','fn','lambda_est','q1','q2','q3','p_val']] if not model_config.ponly_run else []
    gamefiles = [f for f in os.listdir(cache_path) if l2_soln in f] if not model_config.ponly_run else [f for f in os.listdir(cache_path) if 'ponly' in f]
    prog_ctr,n_games = 0,len(gamefiles)
    l1R_nr_ct = 0
    emp_acts = load_emp_acts()
    match_table,ci_analysis_table = {},{}
    gamefiles = [x for x in gamefiles if '.'.join(x.split('.')[:-2]) in emp_acts]
    
    for gamefile in gamefiles:
        prog_ctr += 1
        #print('processing',prog_ctr,'/',n_games,gamefile)
        with open(os.path.join(cache_path, gamefile)) as f:
            gametree = json.load(f)
            gametree = {ast.literal_eval(k):v for k,v in gametree.items()}
            payoff_dict = {k:v[1] for k,v in gametree.items()}
            model_config.gamefile = gamefile
            if len(payoff_dict) == 0:
                continue
            if ss_constants.RUNNING_DATASET != 'intersection':
                all_agents = [gamefile.split('_')[1]] + ['-'.join(x.split('-')[:-1]) for x in list(next(iter(payoff_dict)))[1:]]
            else:
                all_agents = [gamefile.split('_')[1],gamefile.split('_')[2]]
            
            eqcore = EquilibriaCore(num_players=len(next(iter(payoff_dict))),pay_off_dict=payoff_dict,N=len(payoff_dict),sv_actions=list(set([x[0] for x in payoff_dict.keys()])),is_l1agent=False)
            emp_act_for_game = get_emp_act_for_game(emp_acts, all_agents, gamefile, 'l2')
            if pref_model_type == 'nash':
                l1_resps = eqcore.calc_level1_response(model_config)
                match = does_match_resp(emp_acts, l1_resps, all_agents, gamefile,payoff_dict)
                update_match_table(match_table,all_agents,match,'l1_resps')
                
                l1R_resps = eqcore.calc_level1R_response(model_config)
                if len(l1R_resps) > 0:
                    match = does_match_resp(emp_acts, l1R_resps, all_agents, gamefile,payoff_dict)
                    update_match_table(match_table,all_agents,match,'l1R_resps')
                else:
                    l1R_nr_ct += 1
                
                rule_resp = ss_utils.get_rule_strat(all_agents)
                match = does_match_resp(emp_acts, [rule_resp], all_agents, gamefile,payoff_dict)
                #match = [(True,0) if rule_resp[_idx] == emp_act_for_game[_idx] else (False, for _idx,_v in enumerate(rule_resp)]
                update_match_table(match_table,all_agents,match,'rule_resp')
                
                
                l2_resps = eqcore.calc_level2_response(model_config)
                match = does_match_resp(emp_acts, l2_resps, all_agents, gamefile,payoff_dict)
                update_match_table(match_table,all_agents,match,'l2_resps')
                
                if ss_constants.RUNNING_DATASET == 'intersection':
                    stack_resps = eqcore.calc_stackelberg_response()
                    match = does_match_resp(emp_acts, stack_resps, all_agents, gamefile,payoff_dict)
                    update_match_table(match_table,all_agents,match,'stack_resps')
                
                # Nash responses
            if len(payoff_dict) < 16:
                nash_resps = eqcore.calc_pure_strategy_nash_equilibrium_exhaustive()
            else:
                clear_cache()
                file_str = '.'.join(os.path.join(cache_path,'cache',gamefile).split('.')[:-1])
                
                eqcore = EquilibriaCore(num_players=len(list(next(iter(payoff_dict)))),pay_off_dict=payoff_dict,N=len(payoff_dict),sv_actions=list(set([x[0] for x in payoff_dict.keys()])),is_l1agent=False)
                eqcore.calc_pure_strategy_nash_equilibrium_gambit(file_str, all_agents)
                player_actions = [list(set([k[i] for k in payoff_dict.keys()])) for i in np.arange(len(all_agents))]
                all_acts = [x for sublist in player_actions for x in sublist]
                nash_resps = {k:v for k,v in payoff_dict.items() if k in eqcore.read_gambit_ne(file_str, all_acts)}
            nash_resps = list(nash_resps.keys())
            if pref_model_type == 'nash':
                if len(nash_resps) > 0:
                    match = does_match_resp(emp_acts, nash_resps, all_agents, gamefile,payoff_dict)
                    update_match_table(match_table,all_agents,match,'nash_resps')
                
            
            if pref_model_type == 'maxmin' and l2_soln == 'maxmin':
                maxmin_resp = eqcore.calc_max_min_response()
                match = does_match_resp(emp_acts, maxmin_resp, all_agents, gamefile,payoff_dict)
                update_match_table(match_table,all_agents,match,'maxmin_resp')
            
            maxmax_resp = eqcore.calc_max_max_response()
            if pref_model_type == 'maxmax' and l2_soln == 'maxmax':
                match = does_match_resp(emp_acts, maxmax_resp, all_agents, gamefile,payoff_dict)
                update_match_table(match_table,all_agents,match,'maxmax_resp')
            
            ci_analysis = CommonInterestAnalysis(maxmax_resp, nash_resps, all_agents, emp_act_for_game, payoff_dict)
            ci_analysis.assign_risk_and_payoff_dominance()
            update_ci_table(ci_analysis_table,all_agents,ci_analysis)
            #print(gamefile,prog_ctr,'/',len(gamefiles))
    
    for models,matches in match_table.items():
        for ag_key, match_l in matches.items():
            match_flag, match_delta = [x[0] for x in match_l], np.asarray([x[1] for x in match_l])
            tp_accu = len([x for x in match_flag if x==ss_constants.TRUE_POSITIVE])/len(match_flag)
            tn_accu = len([x for x in match_flag if x==ss_constants.TRUE_NEGATIVE])/len(match_flag)
            fp_accu = len([x for x in match_flag if x==ss_constants.FALSE_POSITIVE])/len(match_flag)
            fn_accu = len([x for x in match_flag if x==ss_constants.FALSE_NEGATIVE])/len(match_flag)
            match_delta = match_delta[~np.isnan(match_delta)]
            match_delta = match_delta[match_delta > 0]
            
            if len(match_delta) == 0:
                print(models,': ',ag_key,(tp_accu+tn_accu)/(tp_accu+tn_accu+fp_accu+fn_accu),np.inf,None)
                result_rows.append([ag_key,pref_type,pref_model_type,l2_soln if not model_config.ponly_run else 'p',models,tp_accu,tn_accu,fp_accu,fn_accu,np.inf,0,None])
            else:    
                '''
                match_delta = match_delta[np.nonzero(match_delta)]
                delta_hist,delta_bin_edges  = np.histogram(match_delta, bins=20, range=(0,2), density=True)
                delta_bin_edges = [x for x in delta_bin_edges[1:]]
                #delta_hist = delta_hist[::-1]
                #delta_hist = [x/sum(delta_hist) for x in delta_hist]
                #dist = st.expon  
                #loc, scale = dist.fit(delta_hist)
                popt, pcov = curve_fit(lambda m,t,x,b: m * np.exp(-t * x) + b, delta_bin_edges,delta_hist, p0=(1, 1, -.4))
                lambda_est_sd = np.sqrt(sum([(_datay-(popt[0] * np.exp(-popt[1] * _datax) + popt[2]))**2 for _datax,_datay in zip(delta_bin_edges[1:],delta_hist)]))/len(delta_hist)
                lambda_est = -popt[1]
                #lambda_est_sd = np.inf
                #ks_stat = st.kstest(delta_hist, dist.cdf, (loc, scale))
                #lambda_est = scale
                plt.figure()
                plt.plot(delta_bin_edges,delta_hist,'.')
                #plt.plot(delta_bin_edges,[dist.pdf(x, loc, scale) for x in delta_bin_edges])
                plt.plot(delta_bin_edges,[ popt[0] * np.exp(-popt[1] * x) + popt[2] for x in delta_bin_edges])
                #plt.title(str(lambda_est)+', p='+str(ks_stat.pvalue))
                plt.title(str(lambda_est)+','+str(lambda_est_sd))
                plt.show()
                '''
                
                lambda_numeric = 1/np.mean(match_delta)
                #match_delta = match_delta/2
                delta_hist,delta_bin_edges  = np.histogram(match_delta, bins=20, range=(0,2), density=True)
                delta_bin_edges = delta_bin_edges[1:]
                #hist_dist = st.rv_histogram((delta_hist,delta_bin_edges))
                
                #delta_bin_edges = [x for x in delta_bin_edges[1:]]
                #plt.figure()
                #fig = plt.figure()
                #ax1 = fig.add_subplot(121)
                #plt.hist(match_delta, density=True, bins=40)
                #plt.show()
                #delta_hist = delta_hist[::-1]
                #delta_hist = [x/sum(delta_hist) for x in delta_hist]
                
                
                dist = st.expon   
                #match_delta = [1/x for x in match_delta]
                loc, scale = dist.fit(match_delta)
                #delta_hist,delta_bin_edges  = np.histogram(match_delta, bins=20, density=True)
                #ks_stat = st.kstest(match_delta, dist.cdf, (loc, scale))
                fit_sd = np.sqrt(sum([abs(delta_hist[idx]-(dist.pdf(x, loc=loc, scale=scale)*0.1)) for idx,x in enumerate(delta_bin_edges)]))/len(delta_bin_edges)
                #exp_props_cum = dist.cdf(delta_bin_edges,  loc=loc, scale=scale).tolist()
                emp_props_cum = np.cumsum([x/sum(delta_hist) for x in delta_hist])
                #n_samples = len(match_delta)
                #exp_props_diff = [x if idx == 0 else x-exp_props_cum[idx-1] for idx,x in enumerate(exp_props_cum)]
                emp_props_diff = [x if idx == 0 else x-emp_props_cum[idx-1] for idx,x in enumerate(emp_props_cum)]
                #emp_props_diff_counts = [int(x*n_samples) for x in emp_props_diff]
                #exp_props_diff_counts = [int(x*n_samples) for x in exp_props_diff]
                #chisquare_stat = st.chisquare([int(x*n_samples) for idx,x in enumerate(emp_props_diff) if exp_props_diff[idx] != 0],[int(x*n_samples) for x in exp_props_diff if x!=0])
                obs_sample_distr = np.random.choice(delta_bin_edges, 500,p=emp_props_diff)
                #exp_props_diff = [x/sum(exp_props_diff) for x in exp_props_diff]
                #exp_sample_distr = np.random.choice(delta_bin_edges, 500,p=exp_props_diff)
                #ax2 = fig.add_subplot(122)
                #qq_stat_e = st.probplot(match_delta, dist='expon', sparams=(loc, scale), plot=ax2)
                #ax2.set_title('expon '+str(qq_stat_e[1][2]))
                ks_stat = st.kstest(rvs=obs_sample_distr, cdf='truncexpon', args=(2,loc, scale))
                lambda_est = 1/scale if scale !=0 else np.inf
                percentiles = np.percentile(match_delta, [25, 50, 75]).tolist()
                
                #plt.hist(match_delta, density=True, bins=20)
                #plt.hist(obs_sample_distr, density=True, bins=20,color='blue')
                #plt.hist(exp_sample_distr, density=True, bins=20,color='red',alpha=0.5)
                #plt.plot(delta_bin_edges,[dist.pdf(x, loc=loc, scale=scale) for x in delta_bin_edges],color='red')
                #plt.title(str(lambda_est)+','+str(round(ks_stat.pvalue,2))+','+str(round(gte_ks_stat.pvalue,2)))
                #plt.title(str(lambda_est))
                #plt.title(str(lambda_est)+','+str(fit_sd))
                #plt.show()
                
                print(models,': ',ag_key,(tp_accu+tn_accu)/(tp_accu+tn_accu+fp_accu+fn_accu),lambda_est,ks_stat.pvalue)
                result_rows.append([ag_key,pref_type,pref_model_type,l2_soln if not model_config.ponly_run else 'p',models,tp_accu,tn_accu,fp_accu,fn_accu,lambda_est,percentiles[0],percentiles[1],percentiles[2],ks_stat.pvalue])
            
    csv_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','accuracy_data.csv')
    if model_config.model_ctr==0 and os.path.isfile(csv_path):
        os.unlink(csv_path)
        
    with open(csv_path, mode='a') as pref_file:
            sc_writer = csv.writer(pref_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for row in result_rows:
                sc_writer.writerow(row)    
                
    
    result_rows = [['agent','pref_type','pref_model_type','l2_soln','common_interest','payoff_dom','risk_dom']] if not model_config.ponly_run else []
    for ag_key, ci_data_list in ci_analysis_table.items():
        result_rows += [[ag_key,pref_type,pref_model_type,l2_soln if not model_config.ponly_run else 'p']+x for x in ci_data_list]
    csv_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','ci_analysis_data.csv')
    if model_config.model_ctr==0 and os.path.isfile(csv_path):
        os.unlink(csv_path)
        
    with open(csv_path, mode='a') as pref_file:
            sc_writer = csv.writer(pref_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for row in result_rows:
                sc_writer.writerow(row)    
                
    
    
def solve_all_l2games():
    for pref_type in ['baseline','satisfied','weighted']:
        for pref_model in ['maxmin','maxmax','nash']:
            model_config = ModelConfig('roundabout')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            model_config.ponly_run = True
            solve_l2games(model_config)
                    
    for pref_type in ['baseline','satisfied','weighted']:
        for pref_model in ['maxmin','maxmax','nash']:
            model_config = ModelConfig('crosswalk')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            model_config.ponly_run = True
            solve_l2games(model_config)     
    
    for pref_type in ['baseline','satisfied','weighted']:
        for pref_model in ['maxmin','maxmax','nash']:
            model_config = ModelConfig('intersection')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            model_config.ponly_run = True
            solve_l2games(model_config)     
    
            
def estimate_all_preferences():
    for pref_type in ['satisfied']:
        for pref_model in ['nash']:
            model_config = ModelConfig('roundabout')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            estimate_preference(model_config)        
    for pref_type in ['satisfied']:
        for pref_model in ['nash']:
            model_config = ModelConfig('crosswalk')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            estimate_preference(model_config)     
    
    for pref_type in ['satisfied']:
        for pref_model in ['nash']:
            model_config = ModelConfig('intersection')
            model_config.pref_type = pref_type
            model_config.pref_model = pref_model
            estimate_preference(model_config)     
    

def solve_all_l1games():
    
    model_ctr = 0
    for p_only in [False,True]:
        for pref_type in ['baseline','satisfied','weighted']:
            for pref_model in ['nash','maxmax','maxmin']:
                for l2_soln in ['maxmin','maxmax']:
                    model_config = ModelConfig('intersection')
                    model_config.pref_type = pref_type
                    model_config.pref_model = pref_model
                    model_config.l2_soln = l2_soln
                    model_config.model_ctr = model_ctr
                    model_config.ponly_run = p_only
                    solve_l1games(model_config)    
                    model_ctr += 1    
                
    
    model_ctr = 0
    for p_only in [False,True]:
        for pref_type in ['baseline','satisfied','weighted']:
            for pref_model in ['nash','maxmax','maxmin']:
                for l2_soln in ['maxmin','maxmax']:
                    model_config = ModelConfig('crosswalk')
                    model_config.pref_type = pref_type
                    model_config.pref_model = pref_model
                    model_config.l2_soln = l2_soln
                    model_config.model_ctr = model_ctr
                    model_config.ponly_run = p_only
                    solve_l1games(model_config)    
                    model_ctr += 1
    
    model_ctr = 0      
    for p_only in [False,True]:      
        for pref_type in ['baseline','satisfied','weighted']:
            for pref_model in ['nash','maxmax','maxmin']:
                for l2_soln in ['maxmin','maxmax']:
                    model_config = ModelConfig('roundabout')
                    model_config.pref_type = pref_type
                    model_config.pref_model = pref_model
                    model_config.l2_soln = l2_soln
                    model_config.model_ctr = model_ctr
                    model_config.ponly_run = p_only
                    solve_l1games(model_config)    
                    model_ctr += 1
            
if __name__ == '__main__':
    solve_all_l1games()
    
    
    
    