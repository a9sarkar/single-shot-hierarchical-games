'''
Created on Dec 9, 2021

@author: Atrisha
'''

import numpy as np
import operator
import itertools
from subprocess import check_output, CalledProcessError
import os
import ast
import ss_utils
from ss_utils import GametreeUtils
import ss_constants
import sympy.stats
from sympy import Interval, S, FiniteSet, Union
from sympy.stats import ContinuousRV, P, E, Uniform, Normal
from all_utils import lp_utils
from scipy.optimize import NonlinearConstraint, LinearConstraint, minimize, Bounds
import csv
import json
from sklearn import tree
import math

class PredictiveModel():
    
    def build(self,multi_obj_type,model_type):
        csv_path = os.path.join(ss_constants.dataset_home,ss_constants.RUNNING_DATASET+'_dataset',multi_obj_type+'_params_'+model_type+'.csv')
        model_data = dict()
        with open(csv_path, mode='r',newline='\n') as tax_map_file:
            sc_reader = csv.reader(tax_map_file, delimiter=',')
            row_idx = 0
            for row in sc_reader:
                if row_idx == 0:
                    row_idx += 1
                    continue
                ag_key = row[0]
                safety_util = float(row[1])
                parameter = float(row[2])
                velocity = float(row[4])
                if ag_key not in model_data:
                    model_data[ag_key] = ([],[])
                if not np.isnan(safety_util) and not np.isnan(velocity):
                    model_data[ag_key][0].append([safety_util,velocity])
                    model_data[ag_key][1].append([parameter])
                row_idx +=1
        for _m in list(model_data.keys()):
            ag_key, _data = _m, model_data[_m]
            if len(_data[0]) < 100:
                del model_data[ag_key]
                continue
            X = np.reshape(np.asarray(_data[0]), newshape = (len(_data[0]),2))
            Y = np.reshape(np.asarray(_data[1]), newshape = (len(_data[0]),1))
            train_rows = np.random.randint(low=0,high=X.shape[0],size=math.floor(X.shape[0]*0.8))
            X = X[train_rows,:]
            Y = Y[train_rows,:]
            clf = tree.DecisionTreeRegressor(max_depth=5)
            clf = clf.fit(X, Y)
            model_data[ag_key] = clf
        return model_data
            

class PreferenceEstimation():
    
    def __init__(self,file_str,gametree,all_agents,emp_acts, game_type):
        self.file_str = file_str
        self.gametree = gametree
        self.obs_strat = self.get_emp_act_for_game(emp_acts, all_agents, file_str, game_type)
        self.all_agents = all_agents
    
    def get_emp_act_for_game(self, emp_acts, all_agents, gamefile, game_type):   
        act_only_resps = []
        _file_tokens = gamefile.split('.')
        file_key = '.'.join(gamefile.split('.')[:-2]) if game_type =='l2' else '.'.join(gamefile.split('.')[:-1])
        if ss_constants.RUNNING_DATASET == 'intersection':
            int_agents = gamefile.split('_')[1:3]
            this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(int_agents)] if file_key in emp_acts else {}
        else:
            this_game_empact = [emp_acts[file_key][ag] for idx,ag in enumerate(all_agents)] if file_key in emp_acts else {}
        this_game_empact = tuple(['-'.join(x.split('-')[:-1]) for x in this_game_empact])       
        return this_game_empact
        
class interval():
    
    def __init__(self,inp):
        if inp is not None:
            if isinstance(inp, str):
                range = inp[1:-1]
                range = [float(x) for x in range.split(',')]
                self.range = tuple(range)
                self.incl = []
                if inp[0] =='(':
                    self.incl.append('excl')
                else:
                    self.incl.append('incl')
                if inp[0] ==')':
                    self.incl.append('excl')
                else:
                    self.incl.append('incl')
                self.incl = tuple(self.incl)
            else:
                self.range = inp[0]
                self.incl = inp[1]
            if self.range[0] < self.range[1]:
                self._is_valid = True
            elif self.range[0] == self.range[1] and self.incl[0] == 'incl' and self.incl[1] == 'incl':
                self._is_valid = True
            else:
                self._is_valid = False
            
    @property
    def is_valid(self):
        return self._is_valid
                
    def stringify(self):
        _str = []
        if self.incl[0] == 'incl':
            _str.append('[')
        else:
            _str.append('(')
        _str.append(str(self.range[0]))
        _str.append(',')
        _str.append(str(self.range[1]))
        if self.incl[1] == 'incl':
            _str.append(']')
        else:
            _str.append(')')
        return ''.join(_str)
            
    def intersects(self,intvl):
        lopen1 = True if self.incl[0] == 'excl' else False
        ropen1 = True if self.incl[1] == 'excl' else False
        intv1 = Interval(start = self.range[0], end = self.range[1], left_open=lopen1, right_open=ropen1)
        
        lopen2 = True if intvl.incl[0] == 'excl' else False
        ropen2 = True if intvl.incl[1] == 'excl' else False
        intv2 = Interval(start = intvl.range[0], end = intvl.range[1], left_open=lopen2, right_open=ropen2)
        
        int_intvl = intv1.intersect(intv2)
        if int_intvl != S.EmptySet:
            if not isinstance(int_intvl, FiniteSet): 
                lopen,ropen = int_intvl.left_open, int_intvl.right_open
                incl = tuple(['excl' if lopen else 'incl','excl' if ropen else 'incl'])
                _ranges = (int_intvl.left,int_intvl.right)
            else:
                lopen, ropen = False, False
                incl = tuple(['excl' if lopen else 'incl','excl' if ropen else 'incl'])
                finite_vals = [x for x in int_intvl]
                _ranges = (min(finite_vals),max(finite_vals))
            _local_intv_obj = interval([_ranges,incl]) 
            return True, _local_intv_obj
        else:
            return False, None
          
    def sample(self,size):
        lopen1 = True if self.incl[0] == 'excl' else False
        ropen1 = True if self.incl[1] == 'excl' else False
        intv1 = Interval(start = self.range[0], end = self.range[1], left_open=lopen1, right_open=ropen1)
        if self.range[0] != self.range[1]:
            samples = np.random.uniform(low=self.range[0],high=self.range[1],size=size).tolist()
            if self.incl[0] == 'incl':
                samples += [self.range[0]]
            else:
                samples = [x for x in samples if x!=self.range[0]]
            if self.incl[1] == 'incl':
                samples += [self.range[1]]
            else:
                samples = [x for x in samples if x!=self.range[1]]
            samples = list(set(samples))
            samples = [np.mean(samples)]
        else:
            samples = [self.range[0]]
        return samples
    
    
            
        
    
class intervals():
    
    @classmethod
    def fromlist(self,seq_list):
        _intervals = []
        for idx in np.arange(len(seq_list)-1):
            if idx==len(seq_list)-2:
                _i = interval([(seq_list[idx],seq_list[idx+1]),('incl','incl')])
            else:
                _i = interval([(seq_list[idx],seq_list[idx+1]),('incl','excl')])
            _intervals.append(_i)
        return intervals(_intervals)
    
    @classmethod
    def fromStr(cls,inp_str):
        _intervals = []
        _intvl = interval(inp_str)
        _intervals.append(_intvl)
        return cls(_intervals)
    
    @classmethod
    def fromNumSeq(cls,seq_list):
        _intervals = []
        for idx in np.arange(len(seq_list)-1):
            if idx==len(seq_list)-2:
                _i = interval([(seq_list[idx],seq_list[idx+1]),('incl','incl')])
            else:
                _i = interval([(seq_list[idx],seq_list[idx+1]),('incl','excl')])
            _intervals.append(_i)
        return cls(_intervals)
    
    def __init__(self,interval_list):
        self.interval_list = interval_list    
        self.idx = -1
        
    def contain(self,intvl):
        start_in,end_in = False, False
        for this_intvl in self.interval_list:
            if this_intvl.incl[0] == 'excl' and intvl.incl[0] == 'incl':
                if this_intvl.range[0] < intvl.range[0]:
                    start_in = True
            else:
                if this_intvl.range[0] <= intvl.range[0]:
                    start_in = True
            if this_intvl.incl[1] == 'excl' and intvl.incl[1] == 'incl':
                if this_intvl.range[1] > intvl.range[1]:
                    end_in = True
            else:
                if this_intvl.range[1] >= intvl.range[1]:
                    end_in = True
            if start_in and end_in:
                return True
        return False
            
                
        
    def intersect(self,oth_list):
        f=1
        _x = []
        # Split them first
        _split_intvls = self.split(oth_list)
        for _intvl in _split_intvls.interval_list:
            if self.contain(_intvl) and oth_list.contain(_intvl):
                _x.append(_intvl)
        intersect_intvls = intervals(_x)
        return intersect_intvls 
    
    def split(self,oth_list):
        _x = []
        for ol in oth_list.interval_list:
            _x.append(ol.range[0])
            _x.append(ol.range[1])
        for ol in self.interval_list:
            _x.append(ol.range[0])
            _x.append(ol.range[1])
        _x = list(set(_x))
        _x.sort()
        _intervals = intervals.fromNumSeq(_x)
        
        return _intervals
        
class SatisficingPreferenceEstimation(PreferenceEstimation):
    
    def _eval_util(self,util,thresh):
        _evaluated_util = util[1] if util[0] >= thresh else util[0]
        return _evaluated_util 
    
    def _eval_interval(self,obs_util,oth_util,running_interval):
        valid_intervals = []
        for _intvl in running_interval.interval_list:
            mid_thresl = (_intvl.range[0]+_intvl.range[1])/2
            if self._eval_util(obs_util, mid_thresl) >= self._eval_util(oth_util, mid_thresl):
                valid_intervals.append(_intvl)
        valid_intervals = intervals(valid_intervals)
        return valid_intervals
    
    def calc_thresold(self,obs_utils,other_utils):
        oth_utils = [x for x in other_utils if max(x) > min(obs_utils)]
        global_valid_thresholds = [interval('[-1,1]')]
        for oth_util in oth_utils:
            s1,p1,s2,p2 = obs_utils[0],obs_utils[1],oth_util[0],oth_util[1]
            valid_intervals = []
            if s1 >= s2:
                th = interval([(max(s1,s2),1),('excl','incl')])
                if th.is_valid:
                    valid_intervals.append(th)
            if s1 >= p2:
                th = interval([(s1,s2),('excl','incl')])
                if th.is_valid:
                    valid_intervals.append(th)
            if p1 >= p2:
                th = interval([(-1,min(s1,s2)),('incl','incl')])
                if th.is_valid:
                    valid_intervals.append(th)
            if p1 >= s2:
                th = interval([(s2,s1),('excl','incl')])
                if th.is_valid:
                    valid_intervals.append(th)
            _vt = []
            for this_int in valid_intervals:
                for run_int in global_valid_thresholds:
                    does_intersect,int_intvl = this_int.intersects(run_int)
                    if does_intersect:
                        _vt.append(int_intvl)
            global_valid_thresholds = _vt
        return global_valid_thresholds
            
            
            
    def scalarize(self,payoff_dict,gamma_param):    
        return {k:v[0] if v[0] <= gamma_param else v[1] for k,v in payoff_dict.items()}    
    
    def keywithmaxval(self,d):
        """ a) create a list of the dict's keys and values; 
            b) return the key with the max value"""  
        v=np.asarray(list(d.values()))
        k=list(d.keys())
        max_v = max(v)
        indices = np.where(v == max_v)[0]
        return [k[x] for x in indices],max_v
    
    def _tolist(self,u):
        if isinstance(u, Interval):
            return [(float(u.args[0]),float(u.args[1]),u.args[2],u.args[3])]
        elif isinstance(u, list) or isinstance(u, Union):
            _list_rep = []
            u_l = u if isinstance(u, list) else u.args
            for x in u_l:
                if isinstance(x, Interval):
                    _list_rep.append((float(x.args[0]),float(x.args[1]),x.args[2],x.args[3]))
                else:
                    _list_rep.append((float(x.args[0]),float(x.args[0]),False,False))
            return _list_rep
        else:
            if len(u.args) == 0:
                return []
            else:
                return [(float(u.args[0]),float(u.args[0]),False,False)]
    
    def merge(self,intvls):
        """ Union of a list of intervals e.g. [(1,2),(3,4)] """
        intervals = [Interval(intvl.range[0], intvl.range[1], left_open=True if intvl.incl[0] == 'excl' else False, right_open=True if intvl.incl[1] == 'excl' else False) for intvl in intvls]
        u = Union(*intervals)
        union_intvls_aslist = self._tolist(u)
        union_intvls = [interval([(float(x[0]),float(x[1])),('excl' if x[2] else 'incl', 'excl' if x[3] else 'incl')]) if isinstance(x, tuple) else float(x) for x in union_intvls_aslist]
        return union_intvls
    
    def calc_maxmin(self,agent_payoff_dict_scalarized,agidx):
        row_wise_vals = dict()
        for k,v in agent_payoff_dict_scalarized.items():
            if k[agidx] not in row_wise_vals:
                row_wise_vals[k[agidx]] = [v]
            else:
                row_wise_vals[k[agidx]].append(v)
        _minvals = {k:min(v) for k,v in row_wise_vals.items()}
        maxmin_strat, maxmin_val = self.keywithmaxval(_minvals)
        return maxmin_strat[0]
    
    def estimate_preference(self):
        gt_utils = GametreeUtils()
        p_only_tree = gt_utils.get_l1_game_with_only_p(self.gametree)
        
        if ss_constants.RUNNING_DATASET != 'intersection':
            p_only_tree = {tuple([x if idx==0 else x.split('-')[-1] for idx,x in enumerate(list(k))]):v for k,v in p_only_tree.items()}
        thresh_map = {}
        for agidx,ag in enumerate(self.all_agents):
            if self.obs_strat in p_only_tree:
                player_actions = [list(set([k[i] for k in p_only_tree.keys()])) for i in np.arange(len(self.all_agents))][agidx]
                oth_strats = []
                if self.pref_model == 'nash':
                    othag_obs_strat = [x if idx!= agidx else None for idx,x in enumerate(list(self.obs_strat))]
                    for pa in player_actions:
                        if pa != self.obs_strat[agidx]:
                            _s = [x if x is not None else pa for x in othag_obs_strat]
                            oth_strats.append(tuple(_s))
                    max_obs = max(p_only_tree[self.obs_strat][agidx])
                    min_oths = [min(p_only_tree[_s][agidx]) for _s in oth_strats]
                    if all(x<max_obs for x in min_oths):
                        # Necessary and sufficiency condition achieved
                        valid_thresholds = self.calc_thresold(p_only_tree[self.obs_strat][agidx], [p_only_tree[_s][agidx] for _s in oth_strats])
                        valid_thresholds = self.merge(valid_thresholds)
                        if len(valid_thresholds) != 0:
                            valid_thresholds = [x.stringify() for x in valid_thresholds]
                            thresh_map[ag] = valid_thresholds
                elif self.pref_model == 'maxmax':
                    max_obs = max(p_only_tree[self.obs_strat][agidx])
                    min_oths = [min(p_only_tree[_s][agidx]) for _s in oth_strats]
                    if all(x<max_obs for x in min_oths):
                        partition_points = list(set([x[agidx][0] for x in p_only_tree.values()]))
                        partition_points.sort()
                        if partition_points[0] != -1:
                            partition_points = [-1] + partition_points
                        if partition_points[-1] != 1:
                            partition_points += [1]
                        valid_thresholds = []
                        for int_idx,intvl in enumerate(zip(partition_points[:-1], partition_points[1:])):
                            gamma_param_sample = (intvl[1] - intvl[0]) * np.random.sample() + intvl[0]
                            this_player_payoff_dict = {k:v[agidx] for k,v in p_only_tree.items()}
                            scalarized_dict = self.scalarize(this_player_payoff_dict, gamma_param_sample)
                            maxmax_strat,maxmax_val = self.keywithmaxval(scalarized_dict) # this is maxmax solution
                            if maxmax_strat[0][agidx] == self.obs_strat[agidx]: # agent action is the same for all, so we can just take one
                                valid_thresholds.append(interval([intvl,('incl','excl')]))
                        # Check for +1
                        gamma_param_sample = 1
                        this_player_payoff_dict = {k:v[agidx] for k,v in p_only_tree.items()}
                        scalarized_dict = self.scalarize(this_player_payoff_dict, gamma_param_sample)
                        maxmax_strat,maxmax_val = self.keywithmaxval(scalarized_dict) # this is maxmax solution
                        if maxmax_strat[0][agidx] == self.obs_strat[agidx]: # agent action is the same for all, so we can just take one
                            valid_thresholds.append(interval([(1,1),('incl','incl')]))
                        if len(valid_thresholds) > 0:
                            valid_thresholds = self.merge(valid_thresholds)
                            valid_thresholds = [x.stringify() for x in valid_thresholds]
                            thresh_map[ag] = valid_thresholds
                elif self.pref_model == 'maxmin':
                    max_obs = max(p_only_tree[self.obs_strat][agidx])
                    min_oths = [min(p_only_tree[_s][agidx]) for _s in oth_strats]
                    if all(x<max_obs for x in min_oths):
                        partition_points = list(set([x[agidx][0] for x in p_only_tree.values()]))
                        partition_points.sort()
                        if partition_points[0] != -1:
                            partition_points = [-1] + partition_points
                        if partition_points[-1] != 1:
                            partition_points += [1]
                        valid_thresholds = []
                        for int_idx,intvl in enumerate(zip(partition_points[:-1], partition_points[1:])):
                            gamma_param_sample = (intvl[1] - intvl[0]) * np.random.sample() + intvl[0]
                            this_player_payoff_dict = {k:v[agidx] for k,v in p_only_tree.items()}
                            scalarized_dict = self.scalarize(this_player_payoff_dict, gamma_param_sample)
                            maxmin_strat = self.calc_maxmin(scalarized_dict,agidx) # this is maxmax solution
                            if maxmin_strat == self.obs_strat[agidx]: # agent action is the same for all, so we can just take one
                                valid_thresholds.append(interval([intvl,('incl','excl')]))
                        # Check for +1
                        gamma_param_sample = 1
                        this_player_payoff_dict = {k:v[agidx] for k,v in p_only_tree.items()}
                        scalarized_dict = self.scalarize(this_player_payoff_dict, gamma_param_sample)
                        maxmin_strat = self.calc_maxmin(scalarized_dict,agidx) # this is maxmax solution
                        if maxmin_strat == self.obs_strat[agidx]: # agent action is the same for all, so we can just take one
                            valid_thresholds.append(interval([(1,1),('incl','incl')]))
                        if len(valid_thresholds) > 0:
                            valid_thresholds = self.merge(valid_thresholds)
                            valid_thresholds = [x.stringify() for x in valid_thresholds]
                            thresh_map[ag] = valid_thresholds
                else:
                    raise Exception('unimplemented preference model')
                
        self.thresh_map = thresh_map
        self.p_only_tree = p_only_tree
        return thresh_map
    
    def build_param_model(self):
        if not hasattr(self, 'plot_map'):
            self.plot_map = dict()
        print_rows = []
        plot_map = self.plot_map
        dataset_home = ss_constants.dataset_home
        cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
        for k,v in self.thresh_map.items():
            row = [self.file_str,k,v,self.obs_strat]
            _ags = '.'.join(row[0].split('.')[0:-1])
            file_id, princip_ag, time_ts = tuple(_ags.split('_'))
            if row[1].isdigit():
                this_ag = ast.literal_eval(row[1])
            else:
                this_ag = row[1]
                if ss_constants.RUNNING_DATASET == 'crosswalk':
                    this_ag = this_ag.split('-')[1]
                    if 'ext' in this_ag:
                        this_ag = '_'.join(this_ag.split('_')[:-1])
            if isinstance(this_ag, int):
                ag_type = 'principal'
            else:
                ag_type = 'segment'
            params = row[2]
            params = [interval(x) for x in params]
            strat = row[3]
            all_samples = []
            samples = [x.sample(5) for x in params]
            for x in samples:
                all_samples.extend(x)
            p_only_tree = self.p_only_tree
            all_agents = self.all_agents
            obs_strat = self.obs_strat
            if len(obs_strat) == 0:
                continue
            this_ag_idx = all_agents.index(row[1])
            this_ag_util = p_only_tree[obs_strat][this_ag_idx]
            this_ag_action = obs_strat[this_ag_idx]
            ag_ind_var = this_ag_util[0]
            ag_vel = ss_utils.get_agent_velocity(file_id,this_ag,time_ts)
            if ag_type == 'principal':
                if 'principal' not in plot_map:
                    plot_map['principal'] = tuple([list() for i in np.arange(4)])
                plot_map['principal'][0].extend([ag_ind_var]*len(all_samples))
                plot_map['principal'][1].extend(all_samples)
                plot_map['principal'][2].extend([this_ag_action]*len(all_samples))
                plot_map['principal'][3].extend([ag_vel]*len(all_samples))
            else:
                if ss_constants.RUNNING_DATASET == 'roundabout':
                    ag_key = 'feeder2' if 'feeder2' in this_ag else '_'.join(this_ag.split('_')[1:])
                else:
                    ag_key = this_ag
                if ag_key not in plot_map:
                    plot_map[ag_key] = tuple([list() for i in np.arange(4)])
                plot_map[ag_key][0].extend([ag_ind_var]*len(all_samples))
                plot_map[ag_key][1].extend(all_samples)
                plot_map[ag_key][2].extend([this_ag_action]*len(all_samples))
                plot_map[ag_key][3].extend([ag_vel]*len(all_samples))
        for k,v in plot_map.items():
            x,y,a,vel = v[0],v[1],v[2],v[3]
            for idx in np.arange(len(x)):
                print_rows.append([k,x[idx],y[idx],a[idx],vel[idx]])
        return print_rows
        
    def build_intersection_param_model(self):
        if not hasattr(self, 'plot_map'):
            self.plot_map = dict()
        print_rows = []
        plot_map = self.plot_map
        dataset_home = ss_constants.dataset_home
        cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
        for k,v in self.thresh_map.items():
            row = [self.file_str,k,v,self.obs_strat]
            _row = '.'.join(row[0].split('.')[:-1])
            file_id, princip_ag, oth_ag, time_ts = tuple(_row.split('_'))
            this_ag = ast.literal_eval(row[1])
            if this_ag == int(princip_ag):
                ag_type = 'turning'
            else:
                ag_type = 'straight'
            params = row[2]
            params = [interval(x) for x in params]
            strat = row[3]
            if ss_constants.RUNNING_DATASET == 'intersection':
                this_ag_action = strat[0] if ag_type == 'turning' else strat[1]
            
            all_samples = []
            samples = [x.sample(5) for x in params]
            for x in samples:
                all_samples.extend(x)
            
            p_only_tree = self.p_only_tree
            all_agents = self.all_agents
            obs_strat = self.obs_strat
            this_ag_idx = all_agents.index(row[1])
            this_ag_util = p_only_tree[obs_strat][this_ag_idx]
            ag_ind_var = this_ag_util[0]
            ag_vel = ss_utils.get_agent_velocity(file_id,this_ag,time_ts)
            if ag_type not in plot_map:
                plot_map[ag_type] = tuple([list() for i in np.arange(4)])
            plot_map[ag_type][0].extend([ag_ind_var]*len(all_samples))
            plot_map[ag_type][1].extend(all_samples)
            plot_map[ag_type][2].extend([this_ag_action]*len(all_samples))
            plot_map[ag_type][3].extend([ag_vel]*len(all_samples))
        
        for k,v in plot_map.items():
            x,y,a,vel = v[0],v[1],v[2],v[3]
            for idx in np.arange(len(x)):
                print_rows.append([k,x[idx],y[idx],a[idx],vel[idx]])           
        return print_rows
    
class WeightedPreferenceEstimation(PreferenceEstimation):
    
    def _eval_util(self,util,thresh):
        _evaluated_util = util[1] if util[0] >= thresh else util[0]
        return _evaluated_util 
    
    def formulate_and_solve_lp(self,all_agents,all_obj_mat,all_constr_mat,agidx):
        obj_mat = all_obj_mat[agidx,:]
        constr_mat = [x[agidx,:] for x in all_constr_mat]
        num_params = 2
        solns = lp_utils.solve_lp(obj_mat, constr_mat, num_params)
        # This gives the lower bound, upper bound for safety weight
        solns = list(solns[0])
        if not any(x is None for x in solns):
            solns.sort()
        return solns
    
    
    
         
    
    def estimate_preference(self):
        
        def _sum_utils(W):
            _max_oth_strats = max([W@x for x in self.curr_strat_subsets.values()])
            _max_emps_strat = max([W@x for x in self.curr_objstrat_subsets.values()])
            return _max_emps_strat - _max_oth_strats
        
        def _sum_utils_maxmin(W):
            _max_oth_strats = min([W@x for x in self.curr_strat_subsets.values()])
            _max_emps_strat = min([W@x for x in self.curr_objstrat_subsets.values()])
            return _max_emps_strat - _max_oth_strats
    
        def _stopping_callback(xk,result_state):
            if result_state.nit >= 100:
                return True
            else:
                return False
            
        
        def _minimizing_problem(W):
            _obj_list = []
            ''' we need to change the sign since this is originally a maximization problem'''
            for x in self.curr_objstrat_subsets.values():
                _obj_list.append(W@x)
            return max(_obj_list)
        
        def _maximizing_problem(W):
            _obj_list = []
            for x in self.curr_objstrat_subsets.values():
                _obj_list.append(W@x)
            return -max(_obj_list)
    
        gt_utils = GametreeUtils()
        p_only_tree = gt_utils.get_l1_game_with_only_p(self.gametree)
        
        if ss_constants.RUNNING_DATASET != 'intersection':
            p_only_tree = {tuple([x if idx==0 else x.split('-')[-1] for idx,x in enumerate(list(k))]):v for k,v in p_only_tree.items()}
        thresh_map = {}
        if self.obs_strat in p_only_tree:
            obj_utils = np.asarray(p_only_tree[self.obs_strat])
            for agidx,ag in enumerate(self.all_agents):
                player_actions = [list(set([k[i] for k in p_only_tree.keys()])) for i in np.arange(len(self.all_agents))][agidx]
                oth_strats = []
                if self.pref_model == 'nash':
                    othag_obs_strat = [x if idx!= agidx else None for idx,x in enumerate(list(self.obs_strat))]
                    for pa in player_actions:
                        if pa != self.obs_strat[agidx]:
                            _s = [x if x is not None else pa for x in othag_obs_strat]
                            oth_strats.append(tuple(_s))
                    max_obs = max(p_only_tree[self.obs_strat][agidx])
                    min_oths = [min(p_only_tree[_s][agidx]) for _s in oth_strats]
                    if all(x<max_obs for x in min_oths):
                        # Necessary and sufficiency condition achieved
                        constr_utils = [obj_utils-np.asarray(v) for k,v in p_only_tree.items() if k != self.obs_strat]
                        #constr_utils = [v.tolist() for v in constr_utils]
                        solns = self.formulate_and_solve_lp(self.all_agents,obj_utils,constr_utils,agidx)
                        thresh_map[ag] = solns
                elif self.pref_model == 'maxmax':
                    oth_strats = list([x for x in p_only_tree.keys() if x != self.obs_strat])
                    obj_vect = dict()
                    strat_subsets = {k:np.asarray(v)[agidx,:] for k,v in p_only_tree.items() if k[agidx] == self.obs_strat[agidx]}
                    obj_vect[self.obs_strat[agidx]] = strat_subsets
                    cons_list = []
                    for pl_act in player_actions:
                        if pl_act not in obj_vect:
                            self.curr_strat_subsets = {k:np.asarray(v)[agidx,:] for k,v in p_only_tree.items() if  k[agidx] == pl_act}
                            self.curr_objstrat_subsets = obj_vect[self.obs_strat[agidx]]
                            nlc = NonlinearConstraint(_sum_utils, 0, np.inf)
                            cons_list.append(nlc)
                    if len(self.curr_objstrat_subsets) == 0:
                        continue
                    if any([np.isnan(x).any() or np.isinf(x).any() for x in self.curr_strat_subsets.values()]) or any([np.isnan(x).any() or np.isinf(x).any() for x in self.curr_objstrat_subsets.values()]):
                        continue
                    lc = LinearConstraint(np.asarray([1,1]), 1, 1)
                    cons_list.append(lc)
                    res_obj_high = minimize(fun=_maximizing_problem, x0=np.asarray([0.5, 0.5]), bounds=Bounds(lb=np.asarray([0,0]),ub=np.asarray([1,1])), method='trust-constr', constraints=cons_list, callback = _stopping_callback)
                    res_obj_high.x = np.round(res_obj_high.x,1)
                    res_obj_high.x = [x/np.sum(res_obj_high.x) for x in res_obj_high.x]
                    # We can ignore the lower bound of the feasible region for now.
                    thresh_map[ag] = [res_obj_high.x[0],res_obj_high.x[0]]       
                elif self.pref_model == 'maxmin':
                    oth_strats = list([x for x in p_only_tree.keys() if x != self.obs_strat])
                    obj_vect = dict()
                    strat_subsets = {k:np.asarray(v)[agidx,:] for k,v in p_only_tree.items() if k[agidx] == self.obs_strat[agidx]}
                    obj_vect[self.obs_strat[agidx]] = strat_subsets
                    cons_list = []
                    for pl_act in player_actions:
                        if pl_act not in obj_vect:
                            self.curr_strat_subsets = {k:np.asarray(v)[agidx,:] for k,v in p_only_tree.items() if  k[agidx] == pl_act}
                            self.curr_objstrat_subsets = obj_vect[self.obs_strat[agidx]]
                            nlc = NonlinearConstraint(_sum_utils_maxmin, 0, np.inf)
                            cons_list.append(nlc)
                    if len(self.curr_objstrat_subsets) == 0:
                        continue
                    if any([np.isnan(x).any() or np.isinf(x).any() for x in self.curr_strat_subsets.values()]) or any([np.isnan(x).any() or np.isinf(x).any() for x in self.curr_objstrat_subsets.values()]):
                        continue
                    lc = LinearConstraint(np.asarray([1,1]), 1, 1)
                    cons_list.append(lc)
                    res_obj_high = minimize(fun=_maximizing_problem, x0=np.asarray([0.5, 0.5]), bounds=Bounds(lb=np.asarray([0,0]),ub=np.asarray([1,1])), method='trust-constr', constraints=cons_list, callback = _stopping_callback)
                    res_obj_high.x = np.round(res_obj_high.x,1)
                    res_obj_high.x = [x/np.sum(res_obj_high.x) for x in res_obj_high.x]
                    # We can ignore the lower bound of the feasible region for now.
                    thresh_map[ag] = [res_obj_high.x[0],res_obj_high.x[0]]
                else:
                    raise Exception('unimplemented preference model ' + self.pref_model)
                
        self.thresh_map = thresh_map
        self.p_only_tree = p_only_tree
        return thresh_map
    
    def build_param_model(self):
        if not hasattr(self, 'plot_map'):
            self.plot_map = dict()
        print_rows = []
        plot_map = self.plot_map
        dataset_home = ss_constants.dataset_home
        cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
        for k,v in self.thresh_map.items():
            row = [self.file_str,k,v,self.obs_strat]
            _ags = '.'.join(row[0].split('.')[0:-1])
            file_id, princip_ag, time_ts = tuple(_ags.split('_'))
            if row[1].isdigit():
                this_ag = ast.literal_eval(row[1])
            else:
                this_ag = row[1]
                if ss_constants.RUNNING_DATASET == 'crosswalk':
                    this_ag = this_ag.split('-')[1]
                    if 'ext' in this_ag:
                        this_ag = '_'.join(this_ag.split('_')[:-1])
            if isinstance(this_ag, int):
                ag_type = 'principal'
            else:
                ag_type = 'segment'
            params = row[2]
            all_samples = []
            if any(x is None for x in params):
                samples = []
            else:
                samples = [params[1]]
            all_samples.extend(samples)
            p_only_tree = self.p_only_tree
            all_agents = self.all_agents
            obs_strat = self.obs_strat
            this_ag_idx = all_agents.index(row[1])
            this_ag_util = p_only_tree[obs_strat][this_ag_idx]
            ag_ind_var = this_ag_util[0]
            this_ag_action = obs_strat[this_ag_idx]
            ag_vel = ss_utils.get_agent_velocity(file_id,this_ag,time_ts)
            if ag_type == 'principal':
                if 'principal' not in plot_map:
                    plot_map['principal'] = tuple([list() for i in np.arange(4)])
                plot_map['principal'][0].extend([ag_ind_var]*len(all_samples))
                plot_map['principal'][1].extend(all_samples)
                plot_map['principal'][2].extend([this_ag_action]*len(all_samples))
                plot_map['principal'][3].extend([ag_vel]*len(all_samples))
            else:
                if ss_constants.RUNNING_DATASET == 'roundabout':
                    ag_key = 'feeder2' if 'feeder2' in this_ag else '_'.join(this_ag.split('_')[1:])
                else:
                    ag_key = this_ag
                if ag_key not in plot_map:
                    plot_map[ag_key] = tuple([list() for i in np.arange(4)])
                plot_map[ag_key][0].extend([ag_ind_var]*len(all_samples))
                plot_map[ag_key][1].extend(all_samples)
                plot_map[ag_key][2].extend([this_ag_action]*len(all_samples))
                plot_map[ag_key][3].extend([ag_vel]*len(all_samples))
        
        for k,v in plot_map.items():
            x,y,a,vel = v[0],v[1],v[2],v[3]
            for idx in np.arange(len(x)):
                print_rows.append([k,x[idx],y[idx],a[idx],vel[idx]])
        return print_rows
    
    def build_intersection_param_model(self):
        if not hasattr(self, 'plot_map'):
            self.plot_map = dict()
        print_rows = []
        plot_map = self.plot_map
        dataset_home = ss_constants.dataset_home
        cache_path = os.path.join(dataset_home,ss_constants.RUNNING_DATASET+'_dataset','game_trees')
        for k,v in self.thresh_map.items():
            row = [self.file_str,k,v,self.obs_strat]
            _row = '.'.join(row[0].split('.')[:-1])
            file_id, princip_ag, oth_ag, time_ts = tuple(_row.split('_'))
            this_ag = ast.literal_eval(row[1])
            if this_ag == int(princip_ag):
                ag_type = 'turning'
            else:
                ag_type = 'straight'
            params = row[2]
            strat = row[3]
            if ss_constants.RUNNING_DATASET == 'intersection':
                this_ag_action = strat[0] if ag_type == 'turning' else strat[1]
            all_samples = []
            if any(x is None for x in params):
                all_samples = []
            else:
                all_samples = [params[1]]
            p_only_tree = self.p_only_tree
            all_agents = self.all_agents
            obs_strat = self.obs_strat
            this_ag_idx = all_agents.index(row[1])
            this_ag_util = p_only_tree[obs_strat][this_ag_idx]
            ag_ind_var = this_ag_util[0]
            ag_vel = ss_utils.get_agent_velocity(file_id,this_ag,time_ts)
            if ag_type not in plot_map:
                plot_map[ag_type] = tuple([list() for i in np.arange(4)])
            plot_map[ag_type][0].extend([ag_ind_var]*len(all_samples))
            plot_map[ag_type][1].extend(all_samples)
            plot_map[ag_type][2].extend([this_ag_action]*len(all_samples))
            plot_map[ag_type][3].extend([ag_vel]*len(all_samples))
            
        for k,v in plot_map.items():
            x,y,a,vel = v[0],v[1],v[2],v[3]
            for idx in np.arange(len(x)):
                print_rows.append([k,x[idx],y[idx],a[idx],vel[idx]])
        return print_rows
        