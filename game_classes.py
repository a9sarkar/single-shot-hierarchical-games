'''
Created on Nov 29, 2021

@author: Atrisha
'''
from planners.trajectory_planner import TrajectoryPlanner, VehicleTrajectoryPlanner, PedestrianTrajectoryPlanner, InvalidCenterlineException
from planners.trajectory_planner import WaitTrajectoryConstraints, ProceedTrajectoryConstraints
from planners.planning_objects import TrajectoryConstraintsFactory
from motion_planners.planning_objects import VehicleState
from maps.States import SyntheticScenarioDef, InvalidScenarioException
from shapely.geometry import LineString, Polygon, Point
import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import constants
from maps.States import ScenarioDef
from equilibrium.game_tree import Actions
from visualizer.visualizer import plot_traffic_regions
from rg_visualizer import UniWeberAnalytics
import all_utils
from equilibrium.utilities import Utilities 
import itertools
import copy
from shapely.geometry import LineString
from shapely.affinity import scale
import scipy.special
import rg_constants
import all_utils.utils as base_utils
import ss_constants
import ss_utils
import math
import os
import csv
import ast
import json
from shapely.geometry.linestring import LineString
from roundabout_dataset.rd_utils import ToolUtils
from distributed.profile import process

class GameDefinition():
    
    def __init__(self,file_id, time_ts, veh_id):
        self.time_ts = time_ts
        self.veh_id = veh_id
        self.file_id = file_id
        self.load_principal_agent()
        self.add_clusters()
        
    def assign_emp_manvs(self):
        principal_emp_manv = (None,np.inf)
        principal_agent_emp_track_length = LineString([(x[2],x[3]) for x in self.principal_agent.emp_track]).length
        for manv, manv_types in self.principal_agent.actions.items():
            for manv_type, traj_info in manv_types.items():
                _err = traj_info[0] - principal_agent_emp_track_length
                if np.isinf(principal_emp_manv[1]) or abs(_err) < abs(principal_emp_manv[1]):
                    principal_emp_manv = (manv+'-'+manv_type,_err)
        cl_emp_manv = {x:(None,np.inf) for x in self.clusters.cluster_actions.keys()}
        if ss_constants.RUNNING_DATASET == 'crosswalk':
            cl_emp_track_length = {x:np.mean([y.emp_track_length for y in self.clusters.clusters_info[x]]) for x in self.clusters.cluster_actions.keys()}
            for cl_name, cl_det in self.clusters.cluster_actions.items():
                for manv, manv_types in cl_det.items():
                    for manv_type, traj_info in manv_types.items():
                        _err = traj_info[0] - cl_emp_track_length[cl_name]
                        if np.isinf(cl_emp_manv[cl_name][1]) or abs(_err) < abs(cl_emp_manv[cl_name][1]):
                            cl_emp_manv[cl_name] = (manv+'-'+manv_type,_err)
        elif ss_constants.RUNNING_DATASET == 'roundabout':
            # Since the vehicles can go in different directions, select the empirical track that is closest to the chose cluster representative
            cl_emp_track_length = {x:[y.emp_track_length for y in self.clusters.clusters_info[x]] for x in self.clusters.cluster_actions.keys()}
            for cl_name, cl_det in self.clusters.cluster_actions.items():
                for manv, manv_types in cl_det.items():
                    for manv_type, traj_info in manv_types.items():
                        _errs = [traj_info[0] - x for x in cl_emp_track_length[cl_name]]
                        _errs = sorted(_errs, key=abs)
                        if abs(_errs[0]) >=10:
                            f=1 
                        _err = _errs[0]
                        if np.isinf(cl_emp_manv[cl_name][1]) or abs(_err) < abs(cl_emp_manv[cl_name][1]):
                            cl_emp_manv[cl_name] = (manv+'-'+manv_type,_err)
        else:
            raise Exception("running dataset not set.")
        self.emp_manvs = [principal_emp_manv]
        for pl_idx in np.arange(1,len(self.all_agents)):
            cl_name = self.all_agents[pl_idx]         
            self.emp_manvs.append(cl_emp_manv[cl_name])    
        f=1
    
    def print_game_tree_to_file(self):
        row = []
        row.append(self.file_id)
        row.append(self.time_ts)
        row.append(",".join([str(x) for x in self.all_agents]))
        row.append(",".join([x[0] for x in self.emp_manvs]))
        row.append(",".join([str(x[1]) for x in self.emp_manvs]))
        cache_path = self.cache_path
        with open(os.path.join(cache_path,'_'.join([self.file_id,str(self.principal_agent.agent_id),str(self.time_ts)])+'.json'), 'w') as fp:
            json.dump(self.hierar_game_dict, fp)
        with open(os.path.join('D:\\oneshot_games_data',ss_constants.RUNNING_DATASET+'_dataset','runner_out.csv'), mode='a') as scene_file:
            sc_writer = csv.writer(scene_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            sc_writer.writerow(row)
        
class RoundaboutGameDefinition(GameDefinition):
    
    def plot_generated_trajectories(self,ax):
        for cl_name, cl_agents in self.clusters.clusters_info.items():
            for ag_info in cl_agents:
                for manv, ag_motion in ag_info.agent_motion.items():
                    if 'walk' in manv:
                        continue
                    for mode,traj_list in ag_motion.all_trajectories.items():
                        if len(traj_list) == 0:
                            continue
                        for traj in traj_list:
                            X,Y = [t[1] for t in traj],[t[2] for t in traj]
                            ax.plot(X,Y)
        for manv, ag_motion in self.principal_agent.agent_motion.items(): 
            for mode,traj_list in ag_motion.all_trajectories.items():
                if len(traj_list) == 0:
                    continue
                for traj in traj_list:
                    X,Y = [t[1] for t in traj],[t[2] for t in traj]
                    ax.plot(X,Y,color='black')
    
    def load_principal_agent(self):
        file_id = self.file_id
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        q_string = "select * from TRAJECTORIES WHERE TRACK_ID = "+str(self.veh_id)+" AND TIME BETWEEN "+str(self.time_ts)+" AND "+str(self.time_ts+6)+" ORDER BY TIME"
        c.execute(q_string)
        res = c.fetchall()
        track = []
        for row in res:
            pt_time = row[7]
            traj_time = round(pt_time-self.time_ts,1)
            if traj_time not in [x[0] for x in track]:
                track.append(tuple([traj_time]+list(row[1:])))
        track_ext = []
        if len(track) < 2:
            raise InvalidScenarioException("track too short in db")
        if track[-1][0] < 5.9:
            term_vel = track[-1][4]/3.6
            extr_dist = 0
            for time_step in np.arange(track[-1][0]+.1,6.1,.1):
                _step_dist = term_vel*(time_step-track[-1][0])
                extr_pt = ss_utils.getExtrapoledPoint((track[-2][2],track[-2][3]), (track[-1][2],track[-1][3]), _step_dist)
                track_row = tuple([time_step,track[-1][1],extr_pt[0],extr_pt[1],track[-1][4],0,0,track[0][7]+time_step,track[-1][8],track[-1][9]])
                track_ext.append(track_row)
        princip_agent_track = track + track_ext
        principal_agent = RoundAboutAgentInfo(self.veh_id,'vehicle', self.file_id)
        if len(track_ext) == 0:
            principal_agent.full_track = ss_utils.get_track(self.file_id, self.veh_id, self.time_ts)
            principal_agent.full_track = [tuple([track[-1][0]+(.1*(idx+1))]+list(x)) for idx,x in enumerate(principal_agent.full_track)]
        else:
            principal_agent.full_track = princip_agent_track
        principal_agent.emp_track = princip_agent_track
        principal_agent.origin_direction = principal_agent.emp_track[0][9][0]
        principal_agent.first_segment = ss_utils.get_assigned_segment(self.file_id, self.veh_id, self.time_ts)
        orig = principal_agent.full_track[0][9].split('_')[0]
        principal_agent.waypoints = [(x[2],x[3]) for idx,x in enumerate(principal_agent.full_track) if idx % 10 == 0]
        principal_agent.waypoint_segments = [x[9] for idx,x in enumerate(principal_agent.full_track) if idx % 10 == 0]
        principal_agent.velocity = principal_agent.full_track[0][4]/3.6
        principal_agent.waypoint_vel_samples = [(principal_agent.velocity,)] + [(None,) if i != len(np.arange(1,len(principal_agent.waypoints)-1))//2 else ss_utils.get_reasonable_velocities(principal_agent.waypoint_segments[i],principal_agent) for i in np.arange(1,len(principal_agent.waypoints)-1)] + [ss_utils.get_reasonable_velocities(principal_agent.waypoint_segments[-1], principal_agent)]
        self.principal_agent = principal_agent
        
    ''' 
        Cluster the relevant agents together into appropriate clusters
    '''
    def add_clusters(self):
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,self.file_id+'.db'))
        c = conn.cursor()
        q_string = "select * from RELEVANT_AGENTS where track_id = "+str(self.veh_id)+" and time = "+str(self.time_ts)
        c.execute(q_string)
        res = c.fetchall()
        relev_clusters, rel_vehs = dict(), []
        relev_ag_segs = ss_utils.get_focus_relev_segs(self.principal_agent.first_segment)
        for row in res:
            pt_time, agents = row[1], [int(x) for x in row[2].split(',')]
            for ag in agents:
                ag_seg = ss_utils.get_assigned_segment(self.file_id, ag, self.time_ts)
                if ag_seg is None:
                    continue
                if ag_seg not in relev_ag_segs:
                    continue
                if ag_seg not in relev_clusters:
                    relev_clusters[ag_seg] = [ag]
                else:
                    relev_clusters[ag_seg].append(ag)
        # Add extended cluster regions 
        if not self.principal_agent.first_segment in ['e_to_n_feeder2','w_to_s_feeder2']:
            ext_clusters = ss_constants.roundabout_ext_clusters[self.principal_agent.origin_direction]
            for ext_cl in ext_clusters:
                ext_ags = ss_utils.get_first_agent_on_cluster(self.file_id, ext_cl, self.time_ts)
                if ext_ags is None:
                    continue
                ext_ags = [x[0] for x in ext_ags][0]
                if ext_cl not in relev_clusters:
                    relev_clusters[ext_cl] = [ext_ags]
            
        self.clusters = relev_clusters            
        for cl_name in list(self.clusters.keys()):
            cl_agent_infos = []
            for ag in self.clusters[cl_name]:
                ag_info = RoundAboutAgentInfo(ag,'vehicle',self.file_id)
                ag_info.load_emp_track(self.time_ts)
                if len(ag_info.emp_track[0][9]) > 0:
                    ag_info.origin_direction = ag_info.emp_track[0][9][0]
                else:
                    ag_info.origin_direction = None
                    for tridx in np.arange(len(ag_info.emp_track)):
                        if len(ag_info.emp_track[tridx][9]) > 0:
                            ag_info.origin_direction = ag_info.emp_track[tridx][9][0]
                            break
                    if ag_info.origin_direction is None:
                        raise InvalidScenarioException("agent direction not found")
                orig = ag_info.full_track[0][9].split('_')[0]
                ag_info.waypoints = [(x[2],x[3]) for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                ag_info.waypoint_segments = [x[9] for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                ag_info.velocity = ag_info.full_track[0][4]/3.6
                ag_info.waypoint_vel_samples = [(ag_info.velocity,)] + [(None,) if i != len(np.arange(1,len(ag_info.waypoints)-1))//2 else ss_utils.get_reasonable_velocities(ag_info.waypoint_segments[i],ag_info) for i in np.arange(1,len(ag_info.waypoints)-1)] + [ss_utils.get_reasonable_velocities(ag_info.waypoint_segments[-1], ag_info)]
        
                cl_agent_infos.append(ag_info)
            self.clusters[cl_name] = cl_agent_infos   
            
            
    def generate_actions(self):
        self.generate_principal_agent_action()
        self.generate_relevant_vehicle_actions()    
        
    
    def generate_relevant_vehicle_actions(self):
        prog_idx,N = 0, sum([len(x) for _,x in self.clusters.items()])
        for cl_name, cl_agents in self.clusters.items():
            for ag_info in cl_agents:
                if N > 2:
                    prog_idx += 1
                    print('Generating relev agent trajectory',prog_idx,'/',N)
                ag_info.agent_motion = dict()
                agent_init_velocity_mps = ag_info.emp_track[0][4]/3.6
                agent_waypoints = [(x[2],x[3]) for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                agent_waypoint_segments = [x[9] for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                direction = cl_name
                file_id = self.file_id
                initialize_db = False
                start_ts = self.time_ts
                freq=0.5  
                agent_type = 'vehicle'
                scene_def = SyntheticScenarioDef(ag_info.agent_id, agent_init_velocity_mps, agent_waypoints,agent_waypoint_segments, direction, file_id,initialize_db,start_ts,freq, agent_type)
                manv_map = {'maneuvers':{'wait':None,'proceed':None}}
                for maneuver in list(manv_map['maneuvers'].keys()):
                    constr = TrajectoryConstraintsFactory.get_constraint_object(maneuver, ag_info, None)
                    constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=18,max_acc_lims=5,max_jerk_lims=3)
                    agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver= maneuver, mode=None, horizon=6)
                    agent_motion.agent_state = scene_def.agent
                    agent_motion.generate_trajectory(True)
                    ag_info.agent_motion[maneuver] = copy.deepcopy(agent_motion)
    
    def generate_principal_agent_action(self):
        agent_init_velocity_mps = self.principal_agent.emp_track[0][4]/3.6
        agent_waypoints = self.principal_agent.waypoints
        #plt.figure()
        #plt.plot([x[0] for x in agent_waypoints],[x[1] for x in agent_waypoints],'x')
        #plt.show()
        agent_waypoint_segments = self.principal_agent.waypoint_segments
        origin_direction = self.principal_agent.origin_direction
        file_id = self.file_id
        initialize_db = False
        start_ts = self.time_ts
        freq=0.5 
        agent_type = 'vehicle'
        scene_def = SyntheticScenarioDef(self.veh_id, agent_init_velocity_mps, agent_waypoints,agent_waypoint_segments, origin_direction, file_id,initialize_db,start_ts,freq, agent_type)
        manv_map = {'maneuvers':{'wait':None,'proceed':None}}
        self.principal_agent.agent_motion = dict()
        for manv in list(manv_map['maneuvers'].keys()):
            try:
                constr = TrajectoryConstraintsFactory.get_constraint_object(manv, self.principal_agent, None)
                constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=18,max_acc_lims=5,max_jerk_lims=3)
                constr.principal_agent = self.principal_agent
                agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver= manv, mode=None, horizon=6)
                agent_motion.generate_trajectory(True)
            except InvalidCenterlineException as e:
                raise InvalidScenarioException(str(e))
            self.principal_agent.agent_motion[manv] = copy.deepcopy(agent_motion)
            
    def reformat_cluster_actions(self):
        if len(self.clusters) > 6:
            # If the number of clusters too big, then merge the natural clusters together
            merged_cl_dict = dict()
            tu = ToolUtils()
            processed_clusters = []
            for cl in list(self.clusters.keys()):
                if 'inner_circle' in cl or 'outer_circle' in cl:
                    prev_segs = tu.get_previous(cl)
                    for ps in prev_segs:
                        if ps in self.clusters:
                            if cl in merged_cl_dict:
                                merged_cl_dict[cl] += self.clusters[ps]
                            else:
                                merged_cl_dict[cl] = self.clusters[ps]
                            processed_clusters.append(ps)
                    if cl in merged_cl_dict:
                        merged_cl_dict[cl] += self.clusters[cl]
                    else:
                        merged_cl_dict[cl] = self.clusters[cl]
                    processed_clusters.append(cl)
            for cl in self.clusters.keys():
                if cl not in processed_clusters:
                    merged_cl_dict[cl] = self.clusters[cl]
            self.clusters = merged_cl_dict
        if len(self.clusters) >= 6:
            raise InvalidScenarioException("will deal later")
        for cl_name, cl_agents in self.clusters.items():
            cl_actions = dict()
            for ag_info in cl_agents:
                ag_info.actions = dict()
                for manv, ag_motion in ag_info.agent_motion.items():
                    all_trajs_for_manv = []
                    for mode,traj_list in ag_motion.all_trajectories.items():
                        if len(traj_list) == 0:
                            continue
                        all_trajs_for_manv += traj_list
                    all_trajs_for_manv = [(LineString([(t[1],t[2]) for t in x]).length,x) for x in all_trajs_for_manv]
                    all_trajs_for_manv.sort(key=lambda tup: tup[0])
                    if len(all_trajs_for_manv) < 2:
                        continue
                    # Find the prototype index that is closest to the empirical length for this maneuver
                    # If the difference is too large for this manv (>10m), then just select the mid trajectory.
                    emp_trajls = [x.emp_track_length for x in cl_agents]
                    trajl_errs = [sorted([abs(x[0]-emp_l) for x in all_trajs_for_manv])[0] for emp_l in emp_trajls]
                    if min(trajl_errs) < 10:
                        min_traj_err,sel_pidx = np.inf, None
                        for pidx,trajl in enumerate([x[0] for x in all_trajs_for_manv]):
                            this_min_err = min([abs(trajl-x) for x in emp_trajls])
                            if this_min_err < min_traj_err:
                                min_traj_err = this_min_err
                                sel_pidx = pidx
                        lb,p,ub = all_trajs_for_manv[0],all_trajs_for_manv[sel_pidx],all_trajs_for_manv[-1]
                    else:
                        lb,p,ub = all_trajs_for_manv[0],all_trajs_for_manv[len(all_trajs_for_manv)//2],all_trajs_for_manv[-1]
                    if manv not in ag_info.actions:
                        ag_info.actions[manv] = {'lb':lb,'p':p,'ub':ub}
        clusters_info = Clusters(self.clusters)
        self.clusters = clusters_info
        cluster_actions = dict()
        for cl_name, ag_info_list in self.clusters.clusters_info.items():
            if cl_name not in cluster_actions:
                cluster_actions[cl_name] = dict()
            for ag_info in ag_info_list:
                for manv, acts in ag_info.actions.items():
                    if manv not in cluster_actions[cl_name]:
                        cluster_actions[cl_name][manv] = dict()
                    for traj_type, traj_info in acts.items():
                        if traj_type == 'lb':
                            if traj_type not in cluster_actions[cl_name][manv]:
                                cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                            else:
                                _length = traj_info[0]
                                if _length < cluster_actions[cl_name][manv][traj_type][0]:
                                    cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                        elif traj_type == 'ub':
                            if traj_type not in cluster_actions[cl_name][manv]:
                                cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                            else:
                                _length = traj_info[0]
                                if _length > cluster_actions[cl_name][manv][traj_type][0]:
                                    cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                        else:
                            cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
        self.clusters.cluster_actions = cluster_actions
        self.clusters.cluster_actions = {k:v for k,v in self.clusters.cluster_actions.items() if len(v) != 0}
        if len(self.clusters.cluster_actions) == 0:
            with open(self.file_path, 'w') as fp:
                json.dump({}, fp)
            raise InvalidScenarioException("no relevant clusters")
                
        self.principal_agent.actions = dict()
        for manv, ag_motion in self.principal_agent.agent_motion.items(): 
            all_trajs_for_manv = []
            for mode,traj_list in ag_motion.all_trajectories.items():
                if len(traj_list) == 0:
                    continue       
                all_trajs_for_manv += traj_list
            all_trajs_for_manv = [(LineString([(t[1],t[2]) for t in x]).length,x) for x in all_trajs_for_manv]
            all_trajs_for_manv.sort(key=lambda tup: tup[0])
            if len(all_trajs_for_manv) < 2:
                raise InvalidScenarioException("not enough trajectories for principal agent")
            lb,p,ub = ss_utils.append_assigned_segment(all_trajs_for_manv[0]),\
                        ss_utils.append_assigned_segment(all_trajs_for_manv[len(all_trajs_for_manv)//2]), \
                            ss_utils.append_assigned_segment(all_trajs_for_manv[-1])
            if manv not in self.principal_agent.actions:
                self.principal_agent.actions[manv] = {'lb':lb,'p':p,'ub':ub}
        
    def create_game_tree(self):
        self.reformat_cluster_actions()
        l2tag_map, l2key_map = {'lb':0,'p':1,'ub':2}, {0:'lb',1:'p',2:'ub'}
        all_agents = [self.veh_id] + list(self.clusters.cluster_actions.keys())
        all_manvs = [list(self.principal_agent.agent_motion.keys())]
        self.all_agents = all_agents
        for pl_idx in np.arange(1,len(all_agents)):
            cl_name = all_agents[pl_idx]
            cl_actions = self.clusters.cluster_actions[cl_name]
            actions = [cl_name+'-'+x for x in cl_actions.keys()]
            all_manvs.append(actions)
        l1_keys = list(itertools.product(*[v for v in all_manvs]))
        hierar_game_dict = {str(x):dict() for x in l1_keys}
        size = len(hierar_game_dict)*(3**len(all_agents))
        print('Generating',self.file_id,self.principal_agent.agent_id,self.time_ts,size) 
        
        l2_modifiers = list(itertools.product(*[['lb','p','ub'] for x in np.arange(len(all_agents))]))
        util_object = RoundaboutUtils(self.time_ts,self.file_id,self)
        prog_ctr = 0
        for k in l1_keys:
            if size > 40000:
                prog_ctr += 1
                
            l2_keys = [tuple([k[agidx]+'-'+m[agidx] for agidx in np.arange(len(all_agents))]) for m in l2_modifiers]
            l2_game = {str(x):[] for x in l2_keys}
            l2_prog_ctr,l2_n = 0,len(l2_keys)
            for l2manv in l2_keys:
                if size > 40000:
                    l2_prog_ctr += 1
                    print('size:',size,prog_ctr,'/',len(hierar_game_dict),l2_prog_ctr,'/',l2_n)
                traj_act_comb = []
                for agidx,agid in enumerate(all_agents):
                    traj_type = l2manv[agidx].split('-')[-1]
                    if agidx == 0:
                        # This is the principal agent
                        traj_act_comb.append(self.principal_agent.actions[k[agidx]][traj_type][1])
                    else:
                        cl_name = '-'.join(k[agidx].split('-')[:-1])
                        cl_manv = k[agidx].split('-')[-1]
                        # There is one extra trajectory point in cluster trajectories
                        traj_act_comb.append(self.clusters.cluster_actions[cl_name][cl_manv][traj_type][1][1:])
                util_vect = util_object.assign_utils(l2manv, traj_act_comb)
                l2_game[str(l2manv)] = util_vect
            hierar_game_dict[str(k)] = l2_game            
        self.hierar_game_dict = hierar_game_dict
        self.assign_emp_manvs()
               
class CrosswalkGameDefinition(GameDefinition):
       
    def load_principal_agent(self):
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+str(self.file_id)+'.db'))
        c = conn.cursor()
        q_string = "select * from v_trajectories where track_id = "+str(self.veh_id)+" and time between "+str(self.time_ts)+" and "+str(self.time_ts+6)+" order by time"
        c.execute(q_string)
        res = c.fetchall()
        track = []
        for row in res:
            pt_time = row[6]
            traj_time = round(pt_time-self.time_ts,1)
            if traj_time not in [x[0] for x in track]:
                track.append(tuple([traj_time]+list(row[:8]+row[10:])))
        track_ext = []
        if len(track) < 2:
            raise InvalidScenarioException("track too short in db")
        if track[-1][0] < 6:
            term_vel = track[-1][4]/3.6
            extr_dist = 0
            for time_step in np.arange(track[-1][0]+.1,6.1,.1):
                _step_dist = term_vel*(time_step-track[-1][0])
                extr_pt = ss_utils.getExtrapoledPoint((track[-2][2],track[-2][3]), (track[-1][2],track[-1][3]), _step_dist)
                track_row = tuple([time_step,track[-1][1],extr_pt[0],extr_pt[1],track[-1][4],0,0,track[0][7]+time_step,track[-1][8],track[-1][9]])
                track_ext.append(track_row)
        princip_agent_track = track + track_ext
        principal_agent = CrosswalkAgentInfo(self.veh_id,'vehicle', self.file_id)
        if len(track_ext) == 0:
            principal_agent.full_track = ss_utils.get_track(self.file_id, self.veh_id, self.time_ts)
            principal_agent.full_track = [tuple([track[-1][0]+(.1*(idx+1))]+list(x)) for idx,x in enumerate(principal_agent.full_track)]
        else:
            principal_agent.full_track = princip_agent_track
        
        principal_agent.emp_track = princip_agent_track
        orig = principal_agent.full_track[0][9].split('_')[0]
        direction = orig+'_west' if orig == 'east' else orig+'_east'
        principal_agent.direction = direction
        principal_agent.waypoints = [(x[2],x[3]) for idx,x in enumerate(principal_agent.full_track) if idx % 10 == 0]
        principal_agent.waypoint_segments = [x[9] for idx,x in enumerate(principal_agent.full_track) if idx % 10 == 0]
        principal_agent.velocity = principal_agent.full_track[0][4]/3.6
        principal_agent.waypoint_vel_samples = [(principal_agent.velocity,)] + [(None,) if i != len(np.arange(1,len(principal_agent.waypoints)-1))//2 else ss_utils.get_reasonable_velocities(principal_agent.waypoint_segments[i],principal_agent) for i in np.arange(1,len(principal_agent.waypoints)-1)] + [ss_utils.get_reasonable_velocities(principal_agent.waypoint_segments[-1], principal_agent)]
            
        self.principal_agent = principal_agent
            
    ''' 
        Cluster the relevant agents together into appropriate clusters
    '''
    def add_clusters(self):
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+self.file_id+'.db'))
        c = conn.cursor()
        q_string = "select * from RELEVANT_AGENTS where track_id = "+str(self.veh_id)+" and time between "+str(self.time_ts)+" and "+str(self.time_ts+6)+" and relation_type != 'leading_vehicle';"
        c.execute(q_string)
        res = c.fetchall()
        ped_clusters, peds = dict(), []
        for row in res:
            pt_time, cluster_name, agents = row[1], row[3], [int(x) for x in row[2].split(',')]
            if pt_time == self.time_ts:
                if cluster_name not in ped_clusters:
                    ped_clusters[cluster_name] = agents
                else:
                    ped_clusters[cluster_name] += [agents]
                for x in agents:
                    if x not in peds:
                        peds.append(x)
            else:
                cluster_name = ss_utils.assign_ext_cluster(cluster_name)+'_ext'
                for x in agents:
                    if x not in peds:
                        if cluster_name not in ped_clusters:
                            ped_clusters[cluster_name] = [x]
                        else:
                            ped_clusters[cluster_name] += [x]
                        peds.append(x)
        self.clusters = ped_clusters            
        for cl_name in list(self.clusters.keys()):
            cl_agent_infos = []
            for ag in self.clusters[cl_name]:
                ag_info = CrosswalkAgentInfo(ag,'pedestrian',self.file_id)
                ag_info.load_emp_track(self.time_ts)
                cl_agent_infos.append(ag_info)
            self.clusters[cl_name] = cl_agent_infos   
    
    
    def generate_actions(self):
        self.generate_pedestrian_actions()    
        self.generate_vehicle_action()
    
    def generate_vehicle_action(self):
        agent_init_velocity_mps = self.principal_agent.emp_track[0][4]/3.6
        agent_waypoints = self.principal_agent.waypoints
        #plt.figure()
        #plt.plot([x[0] for x in agent_waypoints],[x[1] for x in agent_waypoints],'x')
        #plt.show()
        agent_waypoint_segments = self.principal_agent.waypoint_segments
        direction = self.principal_agent.direction
        file_id = self.file_id
        initialize_db = False
        start_ts = self.time_ts
        freq=0.5 
        agent_type = 'vehicle'
        scene_def = SyntheticScenarioDef(self.veh_id, agent_init_velocity_mps, agent_waypoints,agent_waypoint_segments, direction, file_id,initialize_db,start_ts,freq, agent_type)
        manv_map = {'maneuvers':{'wait':None,'proceed':None}}
        self.principal_agent.agent_motion = dict()
        for manv in list(manv_map['maneuvers'].keys()):
            try:
                constr = TrajectoryConstraintsFactory.get_constraint_object(manv, self.principal_agent, None)
                constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=18,max_acc_lims=5,max_jerk_lims=3)
                constr.principal_agent = self.principal_agent
                agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver= manv, mode=None, horizon=6)
                agent_motion.generate_trajectory(True)
            except InvalidCenterlineException as e:
                raise InvalidScenarioException(str(e))
            self.principal_agent.agent_motion[manv] = copy.deepcopy(agent_motion)
           
    def generate_pedestrian_actions(self):
        for cl_name, cl_agents in self.clusters.items():
            for ag_info in cl_agents:
                ag_info.agent_motion = dict()
                agent_init_velocity_mps = ag_info.emp_track[0][4]/3.6
                agent_waypoints = [(x[2],x[3]) for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                agent_waypoint_segments = [x[9] for idx,x in enumerate(ag_info.full_track) if idx % 10 == 0]
                direction = cl_name
                file_id = self.file_id
                initialize_db = False
                start_ts = self.time_ts
                freq=0.5 
                agent_type = 'pedestrian'
                scene_def = SyntheticScenarioDef(ag_info.agent_id, agent_init_velocity_mps, agent_waypoints,agent_waypoint_segments, direction, file_id,initialize_db,start_ts,freq, agent_type)
                for maneuver in ['ped_walk','ped_wait']:
                    constr = TrajectoryConstraintsFactory.get_pedestrian_constraint_object(maneuver, scene_def.agent)
                    agent_motion = PedestrianTrajectoryPlanner(traj_constr_obj=constr,maneuver= maneuver, mode=None, horizon=6)
                    agent_motion.agent_state = scene_def.agent
                    agent_motion.generate_trajectory(True)
                    ag_info.agent_motion[maneuver] = copy.deepcopy(agent_motion)
        
    
    def plot_generated_trajectories(self,ax):
        for cl_name, cl_agents in self.clusters.clusters_info.items():
            for ag_info in cl_agents:
                for manv, ag_motion in ag_info.agent_motion.items():
                    if 'walk' in manv:
                        continue
                    for mode,traj_list in ag_motion.all_trajectories.items():
                        if len(traj_list) == 0:
                            continue
                        for traj in traj_list:
                            X,Y = [t[1] for t in traj],[t[2] for t in traj]
                            ax.plot(X,Y)
        for manv, ag_motion in self.principal_agent.agent_motion.items(): 
            for mode,traj_list in ag_motion.all_trajectories.items():
                if len(traj_list) == 0:
                    continue
                for traj in traj_list:
                    X,Y = [t[1] for t in traj],[t[2] for t in traj]
                    ax.plot(X,Y,color='black')
    
    
    def animate_generated_trajectories(self):
        trajs =[]
        for cl_name, cl_agents in self.clusters.clusters_info.items():
            for ag_info in cl_agents:
                for manv, ag_motion in ag_info.agent_motion.items():
                    for mode,traj_list in ag_motion.all_trajectories.items():
                        if len(traj_list) == 0:
                            continue
                        for traj in traj_list:
                            X,Y = [t[1] for t in traj],[t[2] for t in traj]
                            trajs.append(list(zip(X,Y)))
        for manv, ag_motion in self.principal_agent.agent_motion.items(): 
            for mode,traj_list in ag_motion.all_trajectories.items():
                if len(traj_list) == 0:
                    continue
                for traj in traj_list:
                    X,Y = [t[1] for t in traj],[t[2] for t in traj]
                    trajs.append(list(zip(X,Y)))
        ss_utils.animate_scene(trajs, None)
    
    def reformat_cluster_actions(self):
        for cl_name, cl_agents in self.clusters.items():
            cl_actions = dict()
            for ag_info in cl_agents:
                ag_info.actions = dict()
                for manv, ag_motion in ag_info.agent_motion.items():
                    all_trajs_for_manv = []
                    for mode,traj_list in ag_motion.all_trajectories.items():
                        if len(traj_list) == 0:
                            continue
                        all_trajs_for_manv += traj_list
                    all_trajs_for_manv = [(LineString([(t[1],t[2]) for t in x]).length,x) for x in all_trajs_for_manv]
                    all_trajs_for_manv.sort(key=lambda tup: tup[0])
                    lb,p,ub = all_trajs_for_manv[0],all_trajs_for_manv[len(all_trajs_for_manv)//2],all_trajs_for_manv[-1]
                    if manv not in ag_info.actions:
                        ag_info.actions[manv] = {'lb':lb,'p':p,'ub':ub}
        clusters_info = Clusters(self.clusters)
        self.clusters = clusters_info
        cluster_actions = dict()
        for cl_name, ag_info_list in self.clusters.clusters_info.items():
            if cl_name in ss_constants.exclusion_clusters[self.principal_agent.direction]:
                continue
            if cl_name not in cluster_actions:
                cluster_actions[cl_name] = dict()
            for ag_info in ag_info_list:
                for manv, acts in ag_info.actions.items():
                    if manv not in cluster_actions[cl_name]:
                        cluster_actions[cl_name][manv] = dict()
                    for traj_type, traj_info in acts.items():
                        
                        if traj_type == 'lb':
                            if traj_type not in cluster_actions[cl_name][manv]:
                                cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                            else:
                                _length = traj_info[0]
                                if _length < cluster_actions[cl_name][manv][traj_type][0]:
                                    cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                        elif traj_type == 'ub':
                            if traj_type not in cluster_actions[cl_name][manv]:
                                cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                            else:
                                _length = traj_info[0]
                                if _length > cluster_actions[cl_name][manv][traj_type][0]:
                                    cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
                        else:
                            cluster_actions[cl_name][manv][traj_type] = ss_utils.append_assigned_segment(traj_info)
        self.clusters.cluster_actions = cluster_actions
        if len(self.clusters.cluster_actions) == 0:
            with open(self.file_path, 'w') as fp:
                json.dump({}, fp)
        
            raise InvalidScenarioException("no relevant clusters")
                
        self.principal_agent.actions = dict()
        for manv, ag_motion in self.principal_agent.agent_motion.items(): 
            all_trajs_for_manv = []
            for mode,traj_list in ag_motion.all_trajectories.items():
                if len(traj_list) == 0:
                    continue       
                all_trajs_for_manv += traj_list
            all_trajs_for_manv = [(LineString([(t[1],t[2]) for t in x]).length,x) for x in all_trajs_for_manv]
            all_trajs_for_manv.sort(key=lambda tup: tup[0])
            lb,p,ub = ss_utils.append_assigned_segment(all_trajs_for_manv[0]),\
                        ss_utils.append_assigned_segment(all_trajs_for_manv[len(all_trajs_for_manv)//2]), \
                            ss_utils.append_assigned_segment(all_trajs_for_manv[-1])
            if manv not in self.principal_agent.actions:
                self.principal_agent.actions[manv] = {'lb':lb,'p':p,'ub':ub}
        
    def create_game_tree(self):
        self.reformat_cluster_actions()
        l2tag_map, l2key_map = {'lb':0,'p':1,'ub':2}, {0:'lb',1:'p',2:'ub'}
        all_agents = [self.veh_id] + list(self.clusters.cluster_actions.keys())
        all_manvs = [list(self.principal_agent.agent_motion.keys())]
        self.all_agents = all_agents
        for pl_idx in np.arange(1,len(all_agents)):
            cl_name = all_agents[pl_idx]
            cl_actions = self.clusters.cluster_actions[cl_name]
            actions = [cl_name+'-'+x for x in cl_actions.keys()]
            all_manvs.append(actions)
        l1_keys = list(itertools.product(*[v for v in all_manvs]))
        hierar_game_dict = {str(x):dict() for x in l1_keys}
        size = len(hierar_game_dict)*(3**len(all_agents))
        print('Generating',self.file_id,self.principal_agent.agent_id,self.time_ts,size) 
        
        l2_modifiers = list(itertools.product(*[['lb','p','ub'] for x in np.arange(len(all_agents))]))
        util_object = CrosswalkUtils(self.time_ts,self.file_id)
        prog_ctr = 0
        for k in l1_keys:
            if size > 100000:
                prog_ctr += 1
                print(prog_ctr,'/',len(hierar_game_dict))
            l2_keys = [tuple([k[agidx]+'-'+m[agidx] for agidx in np.arange(len(all_agents))]) for m in l2_modifiers]
            l2_game = {str(x):[] for x in l2_keys}
            for l2manv in l2_keys:
                traj_act_comb = []
                for agidx,agid in enumerate(all_agents):
                    traj_type = l2manv[agidx].split('-')[-1]
                    if agidx == 0:
                        # This is the principal agent
                        traj_act_comb.append(self.principal_agent.actions[k[agidx]][traj_type][1])
                    else:
                        cl_name = '-'.join(k[agidx].split('-')[:-1])
                        cl_manv = k[agidx].split('-')[-1]
                        # There is one extra trajectory point in cluster trajectories
                        traj_act_comb.append(self.clusters.cluster_actions[cl_name][cl_manv][traj_type][1][1:])
                util_vect = util_object.assign_utils(l2manv, traj_act_comb)
                l2_game[str(l2manv)] = util_vect
            hierar_game_dict[str(k)] = l2_game            
        self.hierar_game_dict = hierar_game_dict
        self.assign_emp_manvs()
    
    
        
    
            
        
class Clusters():
    
    def __init__(self,clusters_info):
        self.clusters_info = clusters_info
    
    
class AgentInfo():
    
    def __init__(self,agent_id,agent_type, file_id):
        self.agent_id = agent_id
        self.agent_type = agent_type
        self.file_id = file_id
          
class RoundAboutAgentInfo(AgentInfo):
    
    def load_emp_track(self, time_ts):
        self.time_ts = time_ts
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,self.file_id+'.db'))
        c = conn.cursor()
        q_string = "select * from trajectories where track_id = "+str(self.agent_id)+" and time between "+str(self.time_ts)+" and "+str(self.time_ts+6)+" order by time"
        c.execute(q_string)
        res = c.fetchall()
        track_extr_back,track,track_extr_forw = [],[],[]
        if res[0][7] > self.time_ts:
            # Need to extrapolate backwards in time 
            entry_vel = res[0][4]/3.6
            back_dist = 0
            for time_step in np.arange(0,round(res[0][7]-self.time_ts,1),.1):
                _step_dist = entry_vel*time_step
                extr_pt = ss_utils.getExtrapoledPoint((res[1][2],res[1][3]), (res[0][2],res[0][3]), _step_dist)
                track_row = tuple([time_step,res[0][1],extr_pt[0],extr_pt[1],res[0][4],0,0,self.time_ts+time_step,res[0][8],res[0][9]])
                track_extr_back.append(track_row)
            track += track_extr_back
        for row in res:
            pt_time = row[7]
            traj_time = round(pt_time-self.time_ts,1)
            if traj_time not in [x[0] for x in track]:
                track.append(tuple([traj_time]+list(row[1:])))
        if track[-1][0] < 6:
            term_vel = track[-1][4]/3.6
            extr_dist = 0
            for time_step in np.arange(track[-1][0]+.1,6.1,.1):
                _step_dist = term_vel*(time_step-track[-1][0])
                extr_pt = ss_utils.getExtrapoledPoint((track[-2][2],track[-2][3]), (track[-1][2],track[-1][3]), _step_dist)
                track_row = tuple([round(time_step,1),track[-1][1],extr_pt[0],extr_pt[1],track[-1][4],0,0,track[0][7]+time_step,track[-1][8],track[-1][9]])
                track_extr_forw.append(track_row)
            track += track_extr_forw
        self.emp_track = track
        self.emp_track_length = LineString([(x[2],x[3]) for x in self.emp_track]).length
        full_track = ss_utils.get_track(self.file_id, self.agent_id, self.time_ts)
        if len(track_extr_back) != 0:
            full_track = [tuple([track_extr_back[-1][0]+(.1*(idx+1))]+list(x)) for idx,x in enumerate(full_track)]
        else:
            full_track = [tuple([.1*idx]+list(x)) for idx,x in enumerate(full_track)]
        self.full_track = track_extr_back + full_track
    
class CrosswalkAgentInfo(AgentInfo):
    
        
    def load_emp_track(self, time_ts):
        self.time_ts = time_ts
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+str(self.file_id)+'.db'))
        c = conn.cursor()
        q_string = "select * from v_trajectories where track_id = "+str(self.agent_id)+" and time between "+str(self.time_ts)+" and "+str(self.time_ts+6)+" order by time"
        c.execute(q_string)
        res = c.fetchall()
        track_extr_back,track,track_extr_forw = [],[],[]
        if res[0][6] > self.time_ts:
            # Need to extrapolate backwards in time 
            entry_vel = res[0][3]/3.6
            back_dist = 0
            for time_step in np.arange(0,round(res[0][6]-self.time_ts,1),.1):
                _step_dist = entry_vel*time_step
                extr_pt = ss_utils.getExtrapoledPoint((res[1][1],res[1][2]), (res[0][1],res[0][2]), _step_dist)
                track_row = tuple([time_step,res[0][1],extr_pt[0],extr_pt[1],res[0][3],0,0,self.time_ts+time_step,res[0][7],res[0][10]])
                track_extr_back.append(track_row)
            track += track_extr_back
        for row in res:
            pt_time = row[6]
            traj_time = round(pt_time-self.time_ts,1)
            if traj_time not in [x[0] for x in track]:
                track.append(tuple([traj_time]+list(row[:8]+row[10:])))
        if track[-1][0] < 6:
            term_vel = track[-1][4]/3.6
            extr_dist = 0
            for time_step in np.arange(track[-1][0]+.1,6.1,.1):
                _step_dist = term_vel*(time_step-track[-1][0])
                extr_pt = ss_utils.getExtrapoledPoint((track[-2][2],track[-2][3]), (track[-1][2],track[-1][3]), _step_dist)
                track_row = tuple([round(time_step,1),track[-1][1],extr_pt[0],extr_pt[1],track[-1][4],0,0,track[0][7]+time_step,track[-1][8],track[-1][9]])
                track_extr_forw.append(track_row)
            track += track_extr_forw
        self.emp_track = track
        self.emp_track_length = LineString([(x[2],x[3]) for x in self.emp_track]).length
        full_track = ss_utils.get_track(self.file_id, self.agent_id, self.time_ts)
        if len(track_extr_back) != 0:
            full_track = [tuple([track_extr_back[-1][0]+(.1*(idx+1))]+list(x)) for idx,x in enumerate(full_track)]
        else:
            full_track = [tuple([.1*idx]+list(x)) for idx,x in enumerate(full_track)]
        self.full_track = track_extr_back + full_track

class CrosswalkUtils():
    
    def __init__(self,time_ts,file_id):
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+str(file_id)+'.db'))
        c = conn.cursor()
        q_string = "select min(time),track_id from v_trajectories where (assigned_segment like '%e_w_seg%' or assigned_segment like '%w_e_seg%') and track_id in (select TRACK_ID from TRAJECTORY_MOVEMENTS WHERE TYPE='Pedestrian') group by track_id having min(time) between "+str(time_ts)+" and "+str(time_ts+18)+" order by track_id,time;"
        c.execute(q_string)
        res = c.fetchall()
        next_incoming_time_gaps_ped = [row[0]-time_ts for row in res]
        if len(next_incoming_time_gaps_ped) < 2 or next_incoming_time_gaps_ped[0] >=6 :
            self.veh_wait_progress_val = 6
        else:
            next_incoming_time_gaps_ped = [(next_incoming_time_gaps_ped[idx],next_incoming_time_gaps_ped[idx+1]-next_incoming_time_gaps_ped[idx]) for idx in np.arange(len(next_incoming_time_gaps_ped)-1)]
            next_incoming_time_gaps_ped = [(1/max(x[0],1))*x[1] for x in next_incoming_time_gaps_ped if x[1] >= 4]
            if len(next_incoming_time_gaps_ped) > 0:
                self.veh_wait_progress_val = max(next_incoming_time_gaps_ped)
            else:
                self.veh_wait_progress_val = 0
            
        q_string = "select min(time),track_id from v_trajectories where (assigned_segment like '%east_entry%' or assigned_segment like '%west_entry%') and track_id in (select TRACK_ID from TRAJECTORY_MOVEMENTS WHERE TYPE in ('Car', 'Medium Vehicle','Heavy Vehicle','Bus')) group by track_id having min(time) between "+str(time_ts)+" and "+str(time_ts+18)+" order by track_id,time;"
        c.execute(q_string)
        res = c.fetchall()
        next_incoming_time_gaps_veh = [row[0]-time_ts for row in res]
        if len(next_incoming_time_gaps_veh) < 2 or next_incoming_time_gaps_veh[0] >=6 :
            self.ped_wait_progress_val = 6
        else:
            next_incoming_time_gaps_veh = [(next_incoming_time_gaps_veh[idx],next_incoming_time_gaps_veh[idx+1]-next_incoming_time_gaps_veh[idx]) for idx in np.arange(len(next_incoming_time_gaps_veh)-1)]
            next_incoming_time_gaps_veh = [(1/max(x[0],1))*x[1] for x in next_incoming_time_gaps_veh if x[1] >= 4]
            if len(next_incoming_time_gaps_veh) > 0:
                self.ped_wait_progress_val = max(next_incoming_time_gaps_veh)
            else:
                self.ped_wait_progress_val = 0
        
        self.veh_wait_progress_val = self.veh_wait_progress_val/6 if self.veh_wait_progress_val < 6 else 1
        self.ped_wait_progress_val = self.ped_wait_progress_val/6 if self.ped_wait_progress_val <6 else 1
    
    def plot(self):
        X  = np.arange(0,10,.1)
        Y = [self.gen_dist_utils(x) for x in X]
        plt.plot(X,Y)
        plt.show()
    
    def both_on_xwalk_utils(self,dist):
        parm = 5
        u = scipy.special.erf(2*(dist - parm) / (1.5 * math.sqrt(2)))
        return u
    
    def gen_dist_utils(self,dist):
        k,x0 = 1.1,5
        u = 1/(1+np.exp(-k*(dist-x0)))
        return u
    
    def assign_veh_safety_util(self,manv_comb,traj_comb):
        util_val = None
        seg_seqs = [[x[9] for x in traj] for traj in traj_comb]
        f=1
        trunc_len = min([len(x) for x in seg_seqs])
        seg_seqs = [x[:trunc_len] for x in seg_seqs]
        for idx in np.arange(len(seg_seqs[0])):
            if seg_seqs[0][idx] == 'south_crosswalk' and 'south_crosswalk' in [x[idx] for x in seg_seqs[1:]]:
                util_val = -1
                break
            if seg_seqs[0][idx] == 'north_crosswalk' and 'north_crosswalk' in [x[idx] for x in seg_seqs[1:]]:
                util_val = -1
                break
        if util_val is None and ('south_crosswalk' in seg_seqs[0] or 'north_crosswalk' in seg_seqs[0]):
            crosswalk_idxs = [idx for idx,x in enumerate(seg_seqs[0]) if x == 'south_crosswalk' or x == 'north_crosswalk']
            for cl_idx,cl_segseqs in enumerate(seg_seqs[1:]):
                cl_segseqs_while_veh_on_xwalk = [x for idx,x in enumerate(cl_segseqs) if idx in crosswalk_idxs]
                if 'south_crosswalk' in cl_segseqs_while_veh_on_xwalk or 'north_crosswalk' in cl_segseqs_while_veh_on_xwalk:
                    cl_on_xwalk = [Point(x[1],x[2]) for idx,x in enumerate(traj_comb[1+cl_idx]) if idx in crosswalk_idxs and x[9] in ['south_crosswalk','north_crosswalk']]
                    dist_from_centerline = min([LineString(ss_constants.crosswalk_line).distance(p) for p in cl_on_xwalk])
                    this_cl_util_val = self.both_on_xwalk_utils(dist_from_centerline)
                    if util_val is None or this_cl_util_val < util_val:
                        util_val = this_cl_util_val
        if util_val is None:
            all_trajs = [x[:trunc_len] for x in traj_comb]
            for cl_idx,cl_traj in enumerate(all_trajs[1:]):
                cl_line = LineString([(x[1],x[2]) for x in cl_traj])
                veh_line = LineString([(x[1],x[2]) for x in all_trajs[0]])
                dist = cl_line.distance(veh_line)
                this_cl_util_val = self.gen_dist_utils(dist)
                if util_val is None or this_cl_util_val < util_val:
                        util_val = this_cl_util_val
                    
        return util_val            
            
        
    def assign_cluster_safety_util(self,manv_comb,traj_comb):
        util_val = [None] * (len(manv_comb)-1)
        seg_seqs = [[x[9] for x in traj] for traj in traj_comb]
        f=1
        trunc_len = min([len(x) for x in seg_seqs])
        seg_seqs = [x[:trunc_len] for x in seg_seqs]
        for idx in np.arange(len(seg_seqs[0])):
            for cl_idx in np.arange(len(util_val)):
                if seg_seqs[0][idx] == 'south_crosswalk' and seg_seqs[cl_idx+1][idx] == 'south_crosswalk':
                    util_val[cl_idx] = -1
                    continue
                if seg_seqs[0][idx] == 'north_crosswalk' and seg_seqs[cl_idx+1][idx] == 'north_crosswalk':
                    util_val[cl_idx] = -1
                    continue
        all_trajs = [x[:trunc_len] for x in traj_comb]
        for cl_idx,cl_util_val in enumerate(util_val):
            if cl_util_val is None:
                cl_traj =  all_trajs[cl_idx+1]
                cl_line = LineString([(x[1],x[2]) for x in cl_traj])
                veh_line = LineString([(x[1],x[2]) for x in all_trajs[0]])
                dist = cl_line.distance(veh_line)
                this_cl_util_val = self.gen_dist_utils(dist)
                if cl_util_val is None or this_cl_util_val < cl_util_val:
                        util_val[cl_idx] = this_cl_util_val
        return util_val
    
    def assign_veh_progress_util(self,manv_comb,traj_comb):
        return self.veh_wait_progress_val if 'wait' in manv_comb[0] else 1
    
    def assign_cluster_progress_util(self,manv_comb,traj_comb):
        util_val = [self.ped_wait_progress_val if 'wait' in manv_comb[cl_idx] else 1 for cl_idx in np.arange(len(manv_comb)-1)]
        return util_val
    
    def assign_utils(self,manv_comb,traj_comb):   
        veh_safe = self.assign_veh_safety_util(manv_comb, traj_comb)
        veh_prog = self.assign_veh_progress_util(manv_comb, traj_comb)
        cl_safe = self.assign_cluster_safety_util(manv_comb, traj_comb)
        cl_prog = self.assign_cluster_progress_util(manv_comb, traj_comb)
        util_vect = [[veh_safe,veh_prog]]
        for cl_idx in np.arange(len(manv_comb)-1):
            util_vect.append([cl_safe[cl_idx],cl_prog[cl_idx]])
        return util_vect

class RoundaboutUtils():
    
    def excess_creep_util(self,creep_dist):
        parm = 2.5
        u = -scipy.special.erf(2*((creep_dist/1.8) - parm) / (1.5 * math.sqrt(3)))
        return u
    
    def gen_dist_utils(self,dist,parm=7):
        #parm = 7
        u = scipy.special.erf(1.75*(dist - parm) / (1.5 * math.sqrt(3)))
        return u
    
    def gen_linear_util(self,x,min_val,max_val,xval_at_max):
        if x >= xval_at_max:
            return max_val
        else:
            return min_val + (max(x/xval_at_max,0)*(max_val-min_val))
    
    def plot(self):
        X  = np.arange(0,10,.1)
        Y = [self.gen_dist_utils(x) for x in X]
        plt.plot(X,Y)
        plt.show()
    
    def __init__(self,time_ts,file_id,game_def):
        self.time_ts = time_ts
        self.file_id = file_id
        self.game_def =game_def
        
        tu = ToolUtils()
        dir_map = {'e':'east','w':'west','n':'north','s':'south'}
        density_check_regions = ss_constants.roundabout_ext_clusters[self.game_def.principal_agent.origin_direction][:2]
        next_segs = []
        for s in density_check_regions:
            next_segs += tu.get_next(s)
        density_check_regions += next_segs
        dir_orig = self.game_def.principal_agent.origin_direction
        focus_regions = [dir_map[dir_orig]+'_inner_circle',dir_map[dir_orig]+'_outer_circle']
        density_check_regions += focus_regions
        #if self.game_def.principal_agent.first_segment in ss_constants.roundabout_dedicated_turn_segments:
        #    density_check_regions = [x for x in density_check_regions if 'outer' in x]
        n_relev_agents = ss_utils.get_num_agents_on_regions(density_check_regions, self.file_id, self.time_ts)
        self.veh_wait_progress_val = 1-self.gen_linear_util(x=n_relev_agents, min_val=0, max_val=1,xval_at_max= 4)
        
    
    def assign_veh_safety_util(self, manv_comb, traj_comb):
        util_val = None
        veh_seg = self.game_def.principal_agent.first_segment
        focus_regions = ss_utils.get_focus_for_stopping_segments(veh_seg)
        manv,manv_type = manv_comb[0].split('-')[0], manv_comb[0].split('-')[1]
        if len(list(set([x for x in self.game_def.clusters.cluster_actions.keys()]) & set(focus_regions))) > 0:
            veh_traj = self.game_def.principal_agent.actions[manv][manv_type][1]
            if manv != 'wait':
                util_val = -1
            else:
                first_seg, last_seg = self.game_def.principal_agent.first_segment, veh_traj[-1][9]
                if first_seg in last_seg.split(','):
                    util_val = 1
                else:
                    excess_creep = []
                    for pt in veh_traj[::-1]:
                        if first_seg not in pt[9].split(','):
                            excess_creep.append(pt)
                    excess_creep_length = LineString([(x[1],x[2]) for x in excess_creep]).length
                    util_val = self.excess_creep_util(excess_creep_length)
        else:
            trunc_len = min([len(x) for x in traj_comb])
            all_trajs = [x[:trunc_len] for x in traj_comb]
            for cl_idx,cl_traj in enumerate(all_trajs[1:]):
                cl_line = LineString([(x[1],x[2]) for x in cl_traj])
                veh_line = LineString([(x[1],x[2]) for x in all_trajs[0]])
                dist = cl_line.distance(veh_line)
                this_cl_util_val = self.gen_dist_utils(dist)
                if util_val is None or this_cl_util_val < util_val:
                    util_val = this_cl_util_val
        return util_val
    
    def assign_veh_progress_util(self,manv_comb, traj_comb ):
        return self.veh_wait_progress_val if 'wait' in manv_comb[0] else 1
        
    def assign_cluster_safety_util(self,manv_comb, traj_comb):
        util_val = [None] * (len(manv_comb)-1)
        trunc_len = min([len(x) for x in traj_comb])
        all_trajs = [x[:trunc_len] for x in traj_comb]
        for cl_idx,cl_manv in enumerate(manv_comb[1:]):
            cl_name,manv,manv_type = tuple(cl_manv.split('-'))
            cl_traj = all_trajs[cl_idx+1]
            for other_traj in [x for tridx,x in enumerate(all_trajs) if tridx != cl_idx+1]:
                cl_line = LineString([(x[1],x[2]) for x in cl_traj])
                oth_ag_line = LineString([(x[1],x[2]) for x in other_traj])
                dist = cl_line.distance(oth_ag_line)
                this_cl_util_val = self.gen_dist_utils(dist) if 'feeder' in cl_name else self.gen_dist_utils(dist,2)
                if util_val[cl_idx] is None or this_cl_util_val < util_val[cl_idx]:
                    util_val[cl_idx] = this_cl_util_val
            
        return util_val
    
    def assign_cluster_progress_util(self, manv_comb, traj_comb):
        util_val = [None] * (len(manv_comb)-1)
        trunc_len = min([len(x) for x in traj_comb])
        all_trajs = [x[:trunc_len] for x in traj_comb]
        for cl_idx,cl_manv in enumerate(manv_comb[1:]):
            cl_name,manv,manv_type = tuple(cl_manv.split('-'))
            cl_traj = all_trajs[cl_idx+1]
            if 'feeder' in cl_name:
                if 'wait' in manv_comb[cl_idx+1]:
                    util_val[cl_idx] = 0
                else:
                    util_val[cl_idx] = 1
            else:
                if 'wait' in manv_comb[cl_idx+1]:
                    util_val[cl_idx] = -1
                else:
                    util_val[cl_idx] = 1
        return util_val
    
    def assign_utils(self,manv_comb,traj_comb):   
        veh_safe = self.assign_veh_safety_util(manv_comb, traj_comb)
        veh_prog = self.assign_veh_progress_util(manv_comb, traj_comb)
        cl_safe = self.assign_cluster_safety_util(manv_comb, traj_comb)
        cl_prog = self.assign_cluster_progress_util(manv_comb, traj_comb)
        util_vect = [[veh_safe,veh_prog]]
        for cl_idx in np.arange(len(manv_comb)-1):
            util_vect.append([cl_safe[cl_idx],cl_prog[cl_idx]])
        return util_vect
    
if __name__ == '__main__':
    u = RoundaboutUtils(None,None,None)
    u.plot()