'''
Created on Nov 30, 2021

@author: Atrisha
'''
import ss_constants
import sqlite3
import os
from shapely.geometry import LineString, Polygon, Point
import math
import matplotlib.pyplot as plt
import ast
from roundabout_dataset.rd_utils import ToolUtils
from Cython.Compiler.CythonScope import cython_test_extclass_utility_code
import numpy as np
from all_utils.utils import get_rule_action

def get_reasonable_velocities(seg,ag_obj):
    if ss_constants.RUNNING_DATASET == 'crosswalk':
        target_vels = ss_constants.CROSSWALK_PROCEED_VEL_RANGES[seg] 
        if ag_obj is not None and ag_obj.velocity < 0.5:
            target_vels = (0.5,target_vels[1])
        if ag_obj is not None and ag_obj.velocity < target_vels[0]:
            target_vels = (ag_obj.velocity,target_vels[1])
        return target_vels
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        for s in seg.split(','):
            if 'feeder' in s:
                target_vels = (min(ag_obj.velocity,0.5),12)
                break
            else:
                target_vels = (min(ag_obj.velocity,3),19)
        return target_vels
    else:
        raise Exception("running dataset not set.")

def get_track(file_id,agent_id, start_ts):
    if ss_constants.RUNNING_DATASET == 'crosswalk':
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+str(file_id)+'.db'))
        c = conn.cursor()
        if start_ts is not None:
            q_string = "select * from v_trajectories where track_id = "+str(agent_id)+" and time >= "+str(start_ts)+" order by time"
        else:
            q_string = "select * from v_trajectories where track_id = "+str(agent_id)+" order by time"
        c.execute(q_string)
        res = c.fetchall()
        track = [list(row)[:8]+[row[10]] for row in res]
        return track
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        if start_ts is not None:
            q_string = "select * from trajectories where track_id = "+str(agent_id)+" and time >= "+str(start_ts)+" order by time"
        else:
            q_string = "select * from trajectories where track_id = "+str(agent_id)+" order by time"
        c.execute(q_string)
        res = c.fetchall()
        track = [list(row)[1:] for row in res]
        return track
    else:
        raise Exception("running dataset not set.")

def getExtrapoledPoint(p1,p2,dist):
    'Creates a line extrapoled in p1->p2 direction'
    if dist == 0:
        return p2
    else:
        seg_len = math.hypot(p2[0]-p1[0],p2[1]-p1[1])
        EXTRAPOL_RATIO = (dist+seg_len)/seg_len
        if math.isinf(EXTRAPOL_RATIO):
            return p2
        else:
            a = p1
            b = (p1[0]+EXTRAPOL_RATIO*(p2[0]-p1[0]), p1[1]+EXTRAPOL_RATIO*(p2[1]-p1[1]) )
            return b

def animate_scene(self,trajs_list,im_type=None):
        
    fig, ax = plt.subplots()
    lines = [plt.plot([], [],lw=2)[0] for _ in range(len(trajs_list))] 
    rounds = [plt.plot([], [],'o')[0] for _ in range(len(trajs_list))] 
    #plt.xlim(538780, 538890)
    #plt.ylim(4813970, 4814055)
    
    #plot_traffic_regions(ax)
    if im_type is None:
        plt.xlim(537325, 537320)
        plt.ylim(4813176, 4813208)
    
            #ax.imshow(img, extent = imextent)
        
    # initialization function: plot the background of each frame
    patches = lines + rounds
    def init():
        for ln in patches:
            ln.set_data([], [])
        
        return patches
    
    # animation function.  This is called sequentially
    def animate(i):
        #print('called in loop',i)
        for idx,ln in enumerate(lines):
            if i > len(trajs_list[idx]):
                ln.set_data([], [])
            else:
                ln.set_data([x[0] for x in trajs_list[idx][:i]], [x[1] for x in trajs_list[idx][:i]])
        for idx,ln in enumerate(rounds):
            if i > len(trajs_list[idx]):
                ln.set_data([], [])
            else:
                ln.set_data([trajs_list[idx][i][0]], [trajs_list[idx][i][1]])
        return patches
    
    # call the animator.  blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=max([len(x) for x in trajs_list]), interval=100, blit=True, repeat = True) 
    plt.show()

def load_roundabout_regions():
    conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,'917.db'))
    c = conn.cursor()
    lane_map = dict()
    q_string = "select * from TRAFFIC_REGIONS_DEF"
    c.execute(q_string)
    res = c.fetchall()
    for row in res:
        if row[0] not in lane_map:
            lane_map[row[0]] = dict()
            lane_map[row[0]][row[3]] = list(zip(ast.literal_eval(row[4]),ast.literal_eval(row[5])))
        else:
            lane_map[row[0]][row[3]] = list(zip(ast.literal_eval(row[4]),ast.literal_eval(row[5])))
    for reg in list(lane_map.keys()):
        lb,rb = lane_map[reg]['lane_right_boundary'], lane_map[reg]['lane_left_boundary']
        pl = Polygon([list(x) for x in lb+rb[::-1]])
        lane_map[reg] = pl
    return lane_map

def append_assigned_segment(traj_info):
    if ss_constants.RUNNING_DATASET == 'crosswalk':
        region_polygons = {k:Polygon(list(zip(x[0],x[1]))) for k,x in ss_constants.regions_fixed.items()}
        _new_traj = []
        for tp in traj_info[1]:
            x,y = tp[1],tp[2]
            pt = Point(x,y)
            if region_polygons['south_crosswalk'].contains(pt):
                seg = 'south_crosswalk'
            elif region_polygons['north_crosswalk'].contains(pt):
                seg = 'north_crosswalk' 
            else:
                seg = None
                for k,poly in region_polygons.items():
                    if poly.contains(pt):
                        seg = k
                        break
                if seg is None:
                    seg = 'other'
            _newtp = tuple(list(tp) + [seg])
            _new_traj.append(_newtp)
        return (traj_info[0],_new_traj)
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        region_polygons = load_roundabout_regions()
        _new_traj = []
        for tp in traj_info[1]:
            x,y = tp[1],tp[2]
            pt = Point(x,y)
            seg = ','.join([r for r,p in region_polygons.items() if p.contains(pt)])
            _newtp = tuple(list(tp) + [seg])
            _new_traj.append(_newtp)
        return (traj_info[0],_new_traj)
    else:
        raise Exception("running dataset not set.")
        
def assign_ext_cluster(cl_name):
    mapped_cl_name = '-'.join(cl_name.split('-')[1:])
    dir_prefix = cl_name.split('-')[0]
    ext_cl_map = {"north_decision_region":"north_region",
                    "south_decision_region":"south_region",
                    "north_region":"north_region",
                    "north_sidewalk":"north_region",
                    "south_region":"south_region",
                    "west_exit":"north_region",
                    "west_entry":"south_region",
                    "e_w_seg":"north_region",
                    "w_e_seg":"south_region",
                    "east_entry":"north_region",
                    "east_exit":"south_region"}
    return dir_prefix+'-'+ext_cl_map[mapped_cl_name] if mapped_cl_name in ext_cl_map else cl_name

def get_assigned_segment(file_id,ag_id,time_ts):
    if ss_constants.RUNNING_DATASET == 'roundabout':
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        q_string = "select * from trajectories where track_id="+str(ag_id)+" and time = "+str(time_ts)
        c.execute(q_string)
        res = c.fetchall()
        reg = res[0][9].split(',')
        q_string = "select traffic_segment_seq from trajectory_movements where TRACK_ID="+str(ag_id)
        c.execute(q_string)
        res = c.fetchall()
        seg_seq = res[0][0].split(',')
        common_seq = list(set(reg) & set(seg_seq))
        has_feeder = [x for x in common_seq if 'feeder' in x]
        if len(has_feeder) > 0:
            return has_feeder[0]
        elif len(common_seq) > 0:
            return common_seq[0]
        else:
            return None
        
    else:
        raise Exception("running dataset not set.")
    
def get_first_agent_on_cluster(file_id,seg,time_ts):
    if ss_constants.RUNNING_DATASET == 'roundabout':
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        q_string = "select TRACK_ID,MAX(TIME) from TRAJECTORIES WHERE TRAFFIC_REGIONS LIKE '%"+seg+"%' GROUP BY TRACK_ID HAVING MAX(TIME) BETWEEN "+str(time_ts)+" AND "+str(time_ts+6)
        c.execute(q_string)
        res = c.fetchall()
        if len(res) > 0:
            ags = [(row[0],row[1]) for row in res]
            ags.sort(key=lambda tup: tup[1])
            return ags
        else:
            return None
        
    else:
        raise Exception("running dataset not set.")

def get_agent_velocity(file_id,ag,time_ts):
    if ss_constants.RUNNING_DATASET == 'roundabout':
        conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        if isinstance(ag, int) or ag.isdigit():
            q_string = "select * from TRAJECTORIES where track_id="+str(ag)+" AND ABS(TIME-"+str(time_ts)+") < 1 order by time"
            c.execute(q_string)
            res = c.fetchall()
            vel = res[0][4]/3.6
        else:
            q_string = "select * from TRAJECTORIES where traffic_regions like '%"+str(ag)+"%' AND ABS(TIME-"+str(time_ts)+") < 1"
            c.execute(q_string)
            res = c.fetchall()
            if len(res) > 0:
                vel = np.mean([row[4]/3.6 for row in res])
            else:
                q_string = "select * from TRAJECTORIES where traffic_regions like '%"+str(ag)+"%' AND TIME-"+str(time_ts)+" < 6 AND TIME-"+str(time_ts)+" > 0 "
                c.execute(q_string)
                res = c.fetchall()
                vel = res[0][4]/3.6
        return vel 
    elif ss_constants.RUNNING_DATASET == 'intersection':
        conn = sqlite3.connect('D:\\intersections_dataset\\dataset\\'+file_id+'\\uni_weber_'+file_id+'.db')
        c = conn.cursor()
        q_string = "select * from TRAJECTORIES_0"+file_id+" where track_id="+str(ag)+" AND ABS(TIME-"+str(time_ts)+") < 1 order by time"
        c.execute(q_string)
        res = c.fetchall()
        try:
            vel = res[0][3]/3.6
        except IndexError:
            q_string = "select * from TRAJECTORIES_0"+file_id+" where track_id="+str(ag)+" order by time"
            c.execute(q_string)
            res = c.fetchall()
            vel = res[0][3]/3.6
        return vel
    elif ss_constants.RUNNING_DATASET == 'crosswalk':
        conn = sqlite3.connect(os.path.join(ss_constants.CROSSWALK_DB_PATH_HOME,'crosswalk_'+str(file_id)+'.db'))
        c = conn.cursor()
        if isinstance(ag, int) or ag.isdigit():
            q_string = "select * from v_trajectories where track_id="+str(ag)+" AND ABS(TIME-"+str(time_ts)+") < 1 order by time"
            c.execute(q_string)
            res = c.fetchall()
            vel = res[0][3]/3.6
        else:
            q_string = "select * from v_trajectories where assigned_segment like '%"+str(ag)+"%' AND ABS(TIME-"+str(time_ts)+") < 1"
            c.execute(q_string)
            res = c.fetchall()
            vel = np.mean([row[3]/3.6 for row in res])
        return vel
    else:
        raise Exception("running dataset not set.")
    

def is_final_exit(seg):
    _s = seg.split('_')
    if len(_s) >= 2 and '_'.join(_s[-2:]) in ['exit_outer','exit_inner']:
        return True
    else:
        return False
    
def get_num_agents_on_regions(regs,file_id,time_ts):
    conn = sqlite3.connect(os.path.join(ss_constants.ROUNDABOUT_DB_PATH_HOME,file_id+'.db'))
    c = conn.cursor()
    or_strings = ["TRAFFIC_REGIONS LIKE '%"+x+"%'" for x in regs]
    if len(or_strings) > 1:
        q_string = "select * from TRAJECTORIES WHERE ( "+' OR '.join(or_strings)+' ) AND time = '+str(time_ts)
        c.execute(q_string)
        res = c.fetchall()
        return len(res)
    elif len(or_strings) == 1:
        q_string = 'select * from TRAJECTORIES WHERE '+or_strings[0]+' AND time = '+str(time_ts)
        c.execute(q_string)
        res = c.fetchall()
        return len(res)
    else:
        return 0

def get_focus_for_stopping_segments(seg):
    dir_map = {'e':'east','w':'west','n':'north','s':'south'}
    if seg in ss_constants.roundabout_dedicated_turn_segments:
        dir_orig,dir_dest = seg[0], seg.split('_')[2]
        focus_regions = [dir_map[dir_orig]+'_inner_circle',dir_map[dir_orig]+'_outer_circle']
        focus_regions += [dir_map[dir_dest]+'_exit_arm_inner',dir_map[dir_dest]+'_exit_arm_outer']
    else:
        direction = seg[0]
        focus_regions = [dir_map[direction]+'_inner_circle',dir_map[direction]+'_outer_circle']
    return focus_regions

def get_focus_relev_segs(seg):
    tu = ToolUtils()
    dir_map = {'e':'east','w':'west','n':'north','s':'south'}
    direction = seg[0]
    focus_regions = [dir_map[direction]+'_inner_circle',dir_map[direction]+'_outer_circle']
    for s in focus_regions:
        prevs = tu.get_previous(s)
        for p in prevs:
            if 'entry_arm' in p:
                focus_regions.append(p)
    return focus_regions

def get_rule_strat(all_agents):
    # In all the datasets, the principal agent doesn't have the right of way
    if ss_constants.RUNNING_DATASET == 'crosswalk':
        rule_strat = ['wait']
        for ag in all_agents[1:]:
            rule_strat.append('ped_walk')
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        rule_strat = ['wait']
        non_feeder_cluster = any('feeder' not in x for x in all_agents[1:])
        for agidx,ag in enumerate(all_agents[1:]):
            if 'feeder' in ag:
                dir_map = {'e':'east','w':'west','n':'north','s':'south'}
                ag_focus = [dir_map[ag[0]]+'_inner_circle',dir_map[ag[0]]+'_outer_circle']
                oth_ags = [othag for othag in all_agents[1:] if othag != ag]
                if len(oth_ags) > 0:
                    if any(x in ag_focus for x in oth_ags):
                        rule_strat.append('wait')
                    else:
                        rule_strat.append('proceed')
                else:
                    rule_strat.append('proceed')
            else:
                rule_strat.append('proceed')
    elif ss_constants.RUNNING_DATASET == 'intersection':
        rule_strat = ['wait-for-oncoming', 'track_speed']
    else:
        raise Exception("running dataset not set.")
    return tuple(rule_strat)

def get_agent_key(all_agents):
    ag_keys,this_ag = [], None
    if ss_constants.RUNNING_DATASET == 'intersection':
        ag_keys = ['turning','straight']
    elif ss_constants.RUNNING_DATASET == 'crosswalk':
        for agidx, ag in enumerate(all_agents):
            if agidx == 0:
                ag_keys.append('principal')
            else:
                if 'ext' in ag:
                    this_ag = '_'.join(ag.split('_')[:-1])
                    ag_keys.append(this_ag)
                else:
                    ag_keys.append(ag)
    elif ss_constants.RUNNING_DATASET == 'roundabout':
        for agidx, ag in enumerate(all_agents):
            if agidx == 0:
                ag_keys.append('principal')
            else:
                this_ag = 'feeder2' if 'feeder2' in ag else '_'.join(ag.split('_')[1:])
                ag_keys.append(this_ag)
    else:
        raise Exception('unsupported dataset')
    return ag_keys

class GametreeUtils():
    
    def get_l1_game_with_only_p(self,gametree):
        converted_gametree = dict()
        for l1_manv,l1_tree in gametree.items():
            for l2_manv, utils in l1_tree.items():
                l2_manv_key = ast.literal_eval(l2_manv)
                if all(x[-1] == 'p' for x in list(l2_manv_key)):
                    converted_gametree[ast.literal_eval(l1_manv)] = utils
        return converted_gametree 
    