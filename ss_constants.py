'''
Created on Nov 21, 2021

@author: Atrisha
'''
import numpy as np

RUNNING_DATASET = None
dataset_home = 'D:\\oneshot_games_data'

ROUNDABOUT_ALL_FILE_IDS = [x for x in list(np.arange(917,935)) if x != 928] + list(np.arange(988,997))
ROUNDABOUT_DB_PATH_HOME = 'D:\\roundabout_dataset'

CROSSWALK_ALL_FILE_IDS = [x for x in list(np.arange(591,612)) if x not in  [594, 597, 601, 608]]
CROSSWALK_DB_PATH_HOME = 'D:\\crosswalk-dataset'

VEHICLE_TYPES = ["Medium Vehicle","Car","Heavy Vehicle"]




CROSSWALK_PROCEED_VEL_RANGES = {'e_w_seg':(0.5,9),
                            'east_entry':(0.5,7),
                            'east_exit':(0.5,11),
                            'north_sidewalk':(0.5,4),
                            'w_e_seg':(0.5,11),
                            'west_entry':(0.5,12),
                            'west_exit':(0.5,10)}

regions_fixed = {
                    'west_exit':([537328.41, 537328.78, 537328.95, 537333.11, 537333.2, 537333.09, 537332.88],
                                 [4813173.79, 4813177.17, 4813184.15, 4813184.45, 4813180.63, 4813176.74, 4813174.77]
                ),
                    'west_entry':([537332.88, 537333.09, 537333.2, 537333.11, 537337.5, 537337.54, 537337.25],
                                  [4813174.77, 4813176.74, 4813180.63, 4813184.45, 4813184.67, 4813179.89, 4813175.7]
                ),
                    'e_w_seg':([537328.95, 537328.65, 537327.98, 537326.94, 537330.83, 537332.22, 537332.8, 537333.11],
                               [4813184.15, 4813189.02, 4813193.32, 4813197.69, 4813198.82, 4813192.87, 4813188.65, 4813184.45]
                ),
                    'w_e_seg':([537333.11, 537332.8, 537332.22, 537330.83, 537335.13, 537336.02, 537336.73, 537337.27, 537337.5],
                               [4813184.45, 4813188.65, 4813192.87, 4813198.82, 4813199.97, 4813196.2, 4813192.94, 4813188.69, 4813184.67]
                ),
                    'east_entry':([537326.94, 537326.13, 537324.4, 537322.91, 537327.23, 537328.62, 537329.86, 537330.83],
                                  [4813197.69, 4813200.37, 4813204.94, 4813208.18, 4813208.86, 4813205.69, 4813202.24, 4813198.82]
                ),
                    'east_exit':([537330.83, 537329.86, 537328.62, 537327.23, 537331.78, 537332.83, 537334.06, 537335.13],
                                 [4813198.82, 4813202.24, 4813205.69, 4813208.86, 4813209.6, 4813207.28, 4813203.83, 4813199.97]
                ),
                    'south_crosswalk':([537336.74,537336.16,537332.18,537332.72],[4813191.29,4813195.13,4813193.44,4813189.83]),
                    'north_crosswalk':([537332.72,537332.18,537328.12,537328.65],[4813189.83,4813193.44,4813192.11,4813188.60]),
                }

crosswalk_line = [(537332.18,4813193.44),(537332.72,4813189.83)]

exclusion_clusters = {'west_east':['south_north-e_w_seg','north_crosswalk','south_north-north_region','south_north-north_region_ext','south_north-e_w_seg_ext','north_crosswalk_ext','south_north-north_region_ext'],
                      'east_west':['north_south-w_e_seg','south_crosswalk','north_south-south_region','north_south-south_region_ext','north_south-w_e_seg_ext','south_crosswalk_ext','north_south-south_region_ext','north_south-west_entry','north_south-west_exit']}


roundabout_ext_clusters = {'s':['w_to_e_feeder2','w_to_en_feeder2','west_inner_circle','west_outer_circle'],
                           'e':['s_to_ne_feeder2','s_to_nw_feeder2','south_inner_circle','south_outer_circle'],
                           'w':['n_to_se_feeder2','n_to_sw_feeder2','north_inner_circle','north_outer_circle'],
                           'n':['e_to_w_feeder2','e_to_ws_feeder2','east_inner_circle','east_outer_circle']} 

roundabout_dedicated_turn_segments = ['e_to_n_feeder2','w_to_s_feeder2']

TRUE_POSITIVE = 1
FALSE_POSITIVE = 2
TRUE_NEGATIVE = 3
FALSE_NEGATIVE = 4

WAIT_ACTIONS = ['wait','ped_wait','wait-for-oncoming','decelerate-to-stop']